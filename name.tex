\subsection{What's in a name?}
\label{sec:name}

Throughout this paper we use ``free and open source software'' and its
acronym ``FOSS'' as generic labels, resorting to ``free software'' and
``open source software'' where it is useful to do so --- for example,
to distinguish between these two categories of software. It is however
important to note at the outset that the overlap between the two
categories far outweighs the discrepancies: therefore, bar few
exceptions, the corresponding labels refer to the same body of
computer programs (Section~\ref{sec:practicalities}).

To explain why multiple terms exist, in Section~\ref{sec:definitions}
we introduce two social movements active in the FOSS community,
together with the definition of FOSS provided by each. In
Section~\ref{sec:misconceptions} we outline common misconceptions
linked to the different terms, and we address the resulting
confusion. A summary of key points is in
Section~\ref{sec:name-summary}, including a justification of the
approach to terminology we take in this paper.

\subsubsection{Technical definition(s)}
\label{sec:definitions}

The term ``free and open source software'' is a sort of compromise
between the philosophical and political views of two social
movements \parencite[81]{Stallman_2015_ch-14}: the \emph{free software
  movement}, stewarded by the Free Software
Foundation \parencite{FSF-web}, and the \emph{open source (software)
  movement}, stewarded by the Open Source
Initiative \parencite{OSI-web}. We provide historical context for each
in Section~\ref{sec:history}; for now, they can be briefly
characterized as follows. The free software movement is ``a campaign
for freedom for users of software'' \parencite{GNU-web-manifesto} ---
that is, freedom from restrictions on use, study, modification, and
distribution of software. The open source movement is, effectively,
``a marketing program for free software'' \parencite[7]{Fogel_2017},
centred on the practical advantages that can derive from that freedom.

The Free Software Foundation and the Open Source Initiative provide,
respectively, the Free Software
Definition \parencite{GNU-web-definition} and the Open Source
Definition \parencite{OSI-web-definition}. Each definition reflects
the views of the corresponding movement, outlining the requirements a
computer program must fulfil to qualify as free software, in one case,
and as open source software, in the other.

The requirements outlined in the two definitions are formalized in the
licence that accompanies the software, which technically determines
whether the software is free and/or open source. Generally, then,
\emph{free and open source software} is any computer program released
under a licence that grants users rights to run the program for any
purpose, to study it, to modify it, and to redistribute it in original
or modified form \parencite[3]{Stallman_2015_ch-01}. Software that
does not meet this definition is termed \emph{proprietary} (also
\emph{non-free} and \emph{closed source}, as antonyms to ``free'' and
to ``open source'', respectively). All of these terms apply both to
the software itself and to the licence that accompanies it
(Section~\ref{sec:licences}).

The generic definition of FOSS given above paraphrases the Free
Software Definition, which centres on the \emph{four essential
  freedoms} \parencite[3]{Stallman_2015_ch-01} --- that is, the rights
to run, study, modify, and redistribute a computer program. The Open
Source Definition was derived from this framework in a more or less
direct line of descent
\cites[172--174]{Perens_1999}[77]{Stallman_2015_ch-14}, and it is
equivalent for the purpose of defining FOSS in relation to the
licence. However, it is not as easily paraphrased, nor is it readily
summarized, being a 10-clause ``bill of rights for the computer
user'' \parencite[171]{Perens_1999}, linked to a trademark on the term
``Open Source'' administered by the Open Source Initiative
(Section~\ref{sec:history-1990s-open}).

\subsubsection{Misconceptions}
\label{sec:misconceptions}

We can now proceed to introduce terminology to address key
misconceptions surrounding FOSS. These misconceptions stem from use of
the terms ``free'' and ``open (source)'' in relation to software, and
they are common among supporters and detractors of FOSS alike.

\paragraph{Confusion linked to ``free''}
\label{sec:misconceptions-free}

One misconception arises from ambiguity in the English adjective
``free'', which can refer both to liberty (as in ``freedom'') and to
price (as in ``free of
charge'') \parencite[3]{Stallman_2015_ch-01}. To address this
ambiguity, the French/Spanish adjective ``libre'' is sometimes used in
addition to ``free'', or in alternative to it (\ie \emph{free/libre
  software} or \emph{libre software}; also \emph{free/libre and open
  source software} and its acronyms, \emph{FLOSS} or
\emph{F/LOSS}). In other words, the reference here is to ``free as in
freedom'', rather than to ``free as in gratis'' --- a distinction
colloquially captured by the expression `Think of ``free speech,'' not
``free beer''.' \parencite[43]{Stallman_2015_ch-08}.

The term ``freeware'' is easily confused with ``free software'', but
the two are not synonymous \parencite[73]{Stallman_2015_ch-13}. In
particular, \emph{freeware} and its antonym \emph{payware} are
sometimes used to indicate software that is obtained, respectively, at
no cost and in exchange for money (or, more commonly, software
distributed to users \emph{non-commercially} and
\emph{commercially}). Crucially, FOSS and proprietary software can
both be distributed for free or for a
fee \parencite[43]{Stallman_2015_ch-08}; the difference is as
follows. In the case of FOSS, anyone who has a copy of a computer
program can decide whether to distribute copies of the program, and
whether and how much to charge for them
(Section~\ref{sec:licences}). In the case of proprietary software, the
proprietor has sole discretion as to whether copies of the program can
be distributed, and under what conditions (the proprietor is the
``owner'' of the software --- technically, the holder of the copyright
or, equivalently, the licensor; Section~\ref{sec:copyright}).

Therefore, it is incorrect to juxtapose FOSS and commercial software
as mutually exclusive concepts: there is commercial software that is
FOSS, and there is non-commercial software that is
proprietary \parencite[74]{Stallman_2015_ch-13}. In fact, large
fortunes have been accumulated over the past three decades from the
commercial distribution of FOSS, together with provision of related
services (\eg support).

\paragraph{Confusion linked to ``open (source)''}
\label{sec:misconceptions-open}

A relevant notion here is the distinction between distribution of
software with or without source code. In one case, users have access
to a human-readable version of the software; in the other, access is
restricted to the executable machine code compiled from the source
code. The term \emph{source-available} designates any computer program
for which the source code is available to view. Access to the source
code is a prerequisite for the four essential freedoms, in that it
enables users to readily study and/or modify the program
(Section~\ref{sec:definitions}). By definition, then, all FOSS is
source-available, and any software distributed only in compiled form
is proprietary. However, not all source-available software is free and
open source. For example, if the source code can be viewed for
reference only, then the software is source-available and proprietary,
in that modification and redistribution are not allowed.

Much confusion arises from interpretation of the term ``open source''
to mean, effectively, ``source-available''. To address the confusion,
the Open Source Definition states explicitly at the outset that
``[o]pen source doesn't just mean access to the source
code'' \parencite{OSI-web-definition} --- rather, availability of the
source code is only one of multiple requirements a computer program
must fulfil to qualify as open source software
(Sections~\ref{sec:definitions}).

A related issue is confusion about who has access to the source code.
A common misconception is that ``open source'' implies that the source
code is publicly available. That is not the case --- more narrowly,
the four essential freedoms granted by FOSS apply to users of the
software (technically, the licencees; Section~\ref{sec:copyright}). In
a way, this is a moot point, in that if a computer program is FOSS,
there is nothing to prevent users from redistributing its source code,
and thus from making it available to the public. At the same time, it
is important to understand that public availability of the source code
is not required for the program to qualify as FOSS --- and,
conversely, that not all programs for which the public has access to
the source code are FOSS.

The misplaced notion that FOSS necessarily involves public access to
the source code extends also to the development model. In particular,
another common misconception arises from conflation of ``open source''
with ``open development'', \ie collaborative development of a piece of
software, typically through volunteer effort. The defining features of
FOSS make it especially suited to development in this way, and several
successful FOSS projects have leaned heavily on it (\eg development of
the Linux kernel; Section~\ref{sec:history-1990s-linux}). Indeed, the
Open Source Initiative explicitly highlights this approach to
developing computer programs as one of the key practical advantages
linked to software freedom (Section~\ref{sec:definitions}). However,
as in the case of public access to the source code, open development
is not required for a program to qualify as FOSS --- and, conversely,
not all programs developed in the open are FOSS.

\subsubsection{Summary}
\label{sec:name-summary}

To summarize, free and open source software, or FOSS, is any computer
program that is freely modifiable and redistributable, in the sense
that users are granted rights to run the program for any purpose, to
study it, to modify it, and to redistribute it in original or modified
form. To this end, users must be given access to the software in
human-readable form --- that is, to the program's source code.

Any program that is not freely modifiable and redistributable in this
way is proprietary software. Broadly, whether a program is free and
open source \vs proprietary is determined by the licence that
accompanies it. The licence specifies what restrictions apply to the
program in terms of use, study, modification, and distribution
(Section~\ref{sec:legal}).

Several misconceptions arise from this definition, linked to confusion
in terminology. In particular, the terms ``free'' and ``open
(source)'' lead to misunderstandings about the price of the software,
about access to the source code, and about the development
model. Crucially, whether a program is FOSS or proprietary software is
independent of whether it is distributed for free or for a fee ---
that is, non-commercially or commercially. It is also independent of
whether the source code is open, such that the public has access to
it. Similarly, it is independent of whether development occurs in the
open, \ie involving scrutiny and contributions by users, and by the
broader community, as part of a collaborative effort.

In part, the confusion in terminology reflects the historical tension
between two social movements active in the FOSS community: the free
software movement (Section~\ref{sec:history-1980s-free}) and the open
source movement (Section~\ref{sec:history-1990s-open}). One centres on
freedom, in terms of the rights granted to users to run, study,
modify, and redistribute the software (namely, the ``four essential
freedoms''). The other centres instead on the practical advantages
that can derive from that freedom --- for example, to enable (and,
possibly, motivate) users of a piece of software to work
collaboratively to add a desired feature, or to continually review and
fix security issues.
 
Whatever the differences between the two movements, both are firmly
grounded on the premise that humanity as a whole is better served by
software that is free and open source. Advocates of FOSS generally
fall somewhere on ``a multidimensional
scattering'' \parencite[6]{Fogel_2017} of opinions, underscoring the
perspective of each movement to varying degrees. In the interest of
full disclosure, we note that our philosophical and political stance
tends to gravitate towards the free software portion of that state
space.

In an effort to mitigate the confusion, the approach to terminology we
take in this paper departs somewhat from our own ``FOSS orientation'';
a summary of the approach is in Table~\ref{tab:definitions}. Broadly,
we use ``free and open source'' \vs ``proprietary'' as generic terms,
with specific reference to ``free'' and to ``open source'' in the
context of licences (Sections~\ref{sec:practicalities}). We also
resort to the specific labels in outlining the history of FOSS
(Section~\ref{sec:history}), and to highlight the views of one
movement in particular (Section~\ref{sec:gsl}).

We close by reiterating a point we raised at the beginning of the
section --- namely, that the terms ``free software'' and ``open source
software'' effectively refer to the same body of computer
programs. Therefore, the philosophical and political nuances
underlying variation in terminology are of limited practical relevance
to most, including the intended readers of this paper
(Section~\ref{sec:plan}). In everyday practice, the research community
stands to benefit from FOSS through the rights granted to users of a
program (Section~\ref{sec:legal}) --- not from the specific views of
one movement, nor from subtleties in the use of vocabulary
(see \parencite{Stallman_2015_ch-14} for an alternative take on this
issue). At the same time, exploring the intersection between FOSS and
reproducibility, as we set out to do in this paper
(Section~\ref{sec:plan}), requires clarity on the concepts outlined
here.

\input{definitions-tab}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
