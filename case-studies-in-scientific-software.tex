%%% Local Variables:
%%% TeX-master: "foss-in-scientific-research"
%%% End:

\section{Case study: the GNU Scientific Library}
\label{sec:case-study-the-gnu-scientific-library}

% Our main case study involves the GNU Scientific Library (GSL).

% \begin{quote}
%   \emph{GSL is a collection of routines for numerical computing which
%     have been written from scratch in C, and present a modern
%     Applications Programming Interface (API) for C programmers, allowing
%     wrappers to be written for very high level languages.  The source
%     code is distributed under the GNU General Public
%     License. \autocite{galassi2002gnu}}
% \end{quote}

\subsection{The lacuna}

% The earliest motivation for GSL came in the early 1990s when one of
% the authors of this article (MG), still a graduate student, tried to
% obtain source code for CLAMS.  CLAMS was the ``Common Los Alamos
% Mathematical Software'' libray, one of the ``grand old'' libraries of
% numerical analysis \autocite[section 8, page 5]{rocha1999bits}.  CLAMS
% had been used to produce important calculations in many important
% papers (\eg \cite{reimus1995use}), but the source code was never
% released publicly.  He received bureaucratic answers which boiled down
% to the fact that the maintainers were still deciding what to do with
% the source code.  In the end CLAMS was forgotten, the source not
% released, and it will not be ported to modern operating systems.  Any
% of the key calculations that used CLAMS can now never be reproduced.
% The result is that an important piece of nuerical software was
% misplaced by a bureaucracy that did not know how to think clearly
% about what to do with its own software.  Parts of CLAMS were also in
% the public domain SLATEC library, but today it is not possible to pick
% those parts out to rebuild classic simulation results.

% At that time (early 1990s) the go-to library for numerical computing
% was Netlib \autocite{dongarra2008netlib}, a collection of public
% domain mathematical subroutines written in FORTRAN and generally
% considered to be exhaustively tested and of very high quality.
% Netlib's source had been released before US government laboratories
% started making attempts to commercialize software by not releasing
% source code.

% At the same time the book \emph{Numerical Recipes in C} was a runaway
% success among young scientists because of its clear and pedagogical
% description of a wide swath of algorithms.  But reviews of the code
% quality were uniformly negative.  Lysator, the Academic Computer
% Society, collected reviews from USENET postings and other sources in
% 1996 \autocite{lysator1996ReviewsOfNR}, separating out negative and
% favourable reviews.  Almost all reviews were negative, while the two
% favourable reviews focused on the clarity of the description of the
% algorithms and did not analyze the quality of the code at all.

% From \autocite{buerkle2004WhyNotNR}: \emph{A comparison of many FFT
%   implementations shows that the NR code is much worse than other
%   available software. This would be fine if NR made it clear that
%   their code is a pedagogical demonstration only, but it does not--no
%   mention is given (in my edition of NR) of the better FFT algorithms
%   and implementation strategies that exist.}

% A similar and partially overlapping compilation by Buerkle points out
% that \emph{Published reviews of the book[s] have fallen into two
%   classes: Testimonials and reviews by scientists [including Kenneth
%     Wilson, Nobel Laureate] and engineers tend to extol the broad
%   scope and convenience of the products, without seriously evaluating
%   the quality, while reviews by numerical analysts are very critical
%   of the quality of the discussions and the codes\dots}

% Eventually scientists also started looking at the licensing conditions
% of Numerical Recipes.  This led to a call to boycott the specific
% Numerical Recipes implementations \autocite{weiner2007boycott}.

% All this contributed to a crisis in the early 1990s which had three
% prongs:
% \begin{inparaenum}[(a)]
% \item scientists were beginning to write most of their code in C,
% \item high quality free/open-source libraries such as Netlib had old
%   FORTRAN API conventions and were becoming harder to maintain,
% \item newer C libraries such as those from ``Numerical Recipes in
%   C'' had quality and licensing problems that made them unacceptable
%   for significant scientific software pipelines.
% \end{inparaenum}


% \subsection{The design}
% \label{subsec:the-design}
% In 1996 Galassi and Theiler started the design of the GNU Scientific
% Library and kicked off its implementation, contributing the framework
% and initial modules.  Gough joined the project soon after and
% contributed to the design as well as to an explosion of code.  The
% team started with a written design document, and kept it active for
% several years \autocite{galassi_mark_2020_3818202}.

% A reading of the design document today shows that many of the
% motivations and design choices have survived the test of time.  To
% paraphrase the statements at the start of the Motivation chapter:

% \begin{itemize}
% \item GSL is free as in freedom, and distributed under the GPL.
% \item GSL is written in C using ``modern'' coding conventions.  Note
%   that modern C conventions in the late 1990s are still modern
%   conventions today: the C standards and community have valued
%   stability.
% \item GSL is clearly and pedagogically documented.
% \item GSL uses top quality state-of-the-art algorithms.
% \item GSL is portable and configurable using autoconf and automake.
% \item Basically, GSL is GNUlitically correct.
% \end{itemize}

% The Design chapter adds to these points that:
% \begin{itemize}
% \item GSL will rigorously follow standards (the GNU Coding
%   Standards, name canonicalization, \dots)
% \item The GSL will have a test suite for all routines.
% \end{itemize}

% We will discuss the importance of \emph{longevity} in research
% computing.  In the case of GSL it is still possible to build
% 23-year-old versions of GSL with almost no changes.  A big contributor
% to this is the use of the GNU Autotools
% \autocite{vaughan1999autoconf,galassi2018self}, which help with the
% complexities of porting software to different platforms.

% In 1996 the mandate for complete testing was a new feature of the
% software world: this was a decade before the development of the
% current unit test frameworks, a year before the first industry working
% groups on software testing, three years before the publication of
% ``Extreme Programming Explained'', two years before Hetzel's book
% ``The Growth of Software Testing'', five years before the Agile
% Manifesto.

% In academic software development the notion of a test suite that would
% be run automatically as part of the building of a library was unheard
% of at the time, but the GSL team never lost sight of these design
% principles, even after the founders moved on and new maintainers took
% over.

\subsection{Lessons learned}

% Most scientific publications do not dwell on what went wrong and on
% the methodological ``lessons learned'', so these lessons are often
% part of an oral history.  Not dwelling on what went wrong is the
% correct thing to do for a publication, but it is still important to
% communicate these lessons.

% Two approaches are being taken to address this: younger scientists
% often blog about their research, and the medium of a blog allows for
% ``lessons'' articles.  The other is the creation of ``knowledge base''
% systems, allowing teams of engineers to find past approaches that did
% not pan out.

% The lessons learned from this examination of the motivations, design,
% and development of GSL, as well as the lessons from our historical
% survery, are key to the conclusions we draw in
% Sections~\ref{sec:importance-of-foss-in-reproducible} and
% \ref{sec:prolegomena-for-future-research-software}.
