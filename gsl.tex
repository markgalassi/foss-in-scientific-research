\section{The GNU Scientific Library}
\label{sec:gsl}

We illustrate how the notions introduced in Section~\ref{sec:primer}
apply in the context of research and scholarship with reference to the
GNU Scientific Library (GSL) \parencite{Galassi-et-al_2009,
  Galassi-et-al_2019, GSL-web}, a widely used collection of numerical
routines for scientific computing. This choice of example is
opportunistic: one of us (Galassi) was involved in initial conception
of the library in the second half of the 1990s, and in its subsequent
development over several years. Therefore, we can draw on his
first-hand account of the motivations driving the project, and of how
these informed the design of the library.

This section is in three parts. We begin by sketching the landscape of
resources for scientific computing available in the 1990s
(Section~\ref{sec:landscape}). We then frame GSL against this
landscape, which provided the technical backdrop to conception and
development of the library (Section~\ref{sec:gsl-background}). We
conclude by outlining lessons learnt from the case study
(Section~\ref{sec:lessons}).

\subsection{The landscape}
\label{sec:landscape}

The earliest motivation for development of GSL came in the early 1990s
when Galassi, then a visiting graduate student at Los Alamos National
Laboratory, attempted to obtain source code for the Common Los Alamos
Mathematical Software (CLAMS) library for numerical analysis. Instead
of the code he received a series of bureaucratic answers: in practice,
the maintainers of the library had not yet decided under what terms to
release the software. The CLAMS library included all of the routines
in the public-domain SLATEC library (described below), alongside some
routines unique to Los Alamos National Laboratory
\parencite[p.~8-5]{BITS_1999}. Without the source code it was
impossible to determine which routines came from which codebase. It
was therefore impractical, at best, to reproduce results generated
using the CLAMS library \parencite[\eg][]{Wienke_1985,
  Wienke-et-al_1986, Wienke_2009}.

Further motivation for development of GSL came from the landscape of
software for numerical analysis available in the early to mid-1990s
--- a motley collection of resources varying on several dimensions,
such as code quality and terms of use
\parencite{Galassi-et-al-GSL-design}. We can sketch this landscape
with reference to two of those resources, which sit at opposite ends
of the spectrum for the dimensions most relevant to our discussion.

At one end of the spectrum is the SLATEC library --- in full, the
SLATEC Common Mathematical Subroutine Library, or some variant
thereof \parencite{Vandevender-Haskell_1982, Buzbee_1984,
  Fong-et-al_1993}. The library was developed beginning in the late
1970s across several US government research laboratories: the ``SLA''
in ``SLATEC'' stands for three federal research facilities based in
New Mexico (namely, Sandia National Laboratories, Los Alamos National
Laboratory, and Air Force Weapons Laboratory); ``TEC'' is an acronym
for ``Technical Exchange Committee''. The committee had been
established in 1974 as a loose consortium of the computing departments
of those three sites, to foster the circulation of technical knowledge
across them; it was later expanded to include other members, with
others still joining specifically in the effort to develop the
library \parencite{Vandevender-Haskell_1982, Fong-et-al_1993}.

The effort was driven by the aspiration to procure high-quality
numerical software for use on the various systems, including
supercomputers, in operation at the facilities involved in the
collaboration \parencite{Vandevender-Haskell_1982}. It was framed at
the outset as ``an experiment in resource sharing by the computing
departments of several Department of Energy Laboratories'', with the
aim ``to cooperatively assemble and install at each site a
mathematical subroutine library characterized by portability, good
numerical technology, good documentation, robustness, and quality
assurance'' \parencite[16]{Vandevender-Haskell_1982}.

Multiple versions of the codebase were released between 1982 and
1993 \parencite{Fong-et-al_1993}, ``available to anyone on request [in
the] hope that the library will enjoy usage by universities and
laboratories engaged in scientific
computation'' \parencite[310]{Buzbee_1984}. With the launch of
Netlib \parencite{Dongarra-Grosse_1987, Dongarra-et-al_2008,
  Netlib-web} in 1985, specific packages of the library were
additionally made available via the network. Short for ``network
library'', Netlib had been set up to provide ``quick, easy, and
efficient distribution of public-domain software to the scientific
computing community on an as-needed
basis'' \parencite[403]{Dongarra-Grosse_1987}. The aim was to
facilitate the aggregation of relevant computer programs and other
resources, thereby promoting re-use as best practice among researchers
\parencite{Dongarra-et-al_2008}. The material was initially collected
and distributed via ``electronic
mail'' \parencite{Dongarra-Grosse_1987}, and later predominantly via
the World Wide Web \parencite{Dongarra-et-al_2008}.

At the other end of the spectrum is \citetitle*{Press-et-al_1986}, a
series of books published beginning in
\citeyear{Press-et-al_1986} \parencite{Press-et-al_1986}. The books
covered a broad range of algorithms and methods of scientific
computation. They proved popular with researchers for the informal
style of the prose, and for implementing the procedures discussed in
the text as printed computer
routines \parencite[\eg][]{Frolkovic_1990}. For example, the first
edition featured approximately 200 programs, implemented in both
Fortran and Pascal \parencite[1]{Press-et-al_1986}. The ``package''
also included example books \parencite{Press-et-al_1985_Fortran,
  Press-et-al_1985_Pascal} and diskettes, all available for separate
purchase.

Inevitably, there was a trade-off between breadth and depth of the
material covered in the books, both in thoroughness of the
explanations and in sophistication of the
implementations \parencite{Shampine_1987}. Indeed, experts in
numerical analysis generally considered the
\citetitle{Press-et-al_1986} codebase poor in terms of efficacy,
efficiency, and reliability \parencite[\eg][]{Shampine_1987}.

Yet it became common practice among researchers to incorporate
snippets of \citetitle{Press-et-al_1986} code into software they
developed. This practice was problematic, because releasing any such
software is an infringement of copyright (Section~\ref{sec:legal}). For
example, the licencing terms for the first edition of the book
prohibit distribution of the machine-readable code, whether typed from
the text or inputted from related products: the reader is allowed to
make only one copy of each program, for personal
use \parencite[xiii]{Press-et-al_1986}.

\subsection{Conception and design of GSL}
\label{sec:gsl-background}

The SLATEC library and \citetitle{Press-et-al_1986} both provided
impetus to conception of GSL in the mid-1990s, although pulling in
opposite directions. One was a library of high-quality software, in
the public domain and accessible via the network, and aimed at
specialists in numerical analysis. The other was a codebase of low
quality, available under the restrictive terms of a commercial
proprietary licence, and aimed at non-specialists. More broadly, the
software in Netlib could handle difficult computational problems,
which were out of reach of the small algorithms that researchers would
incorporate as snippets of code from the \citetitle{Press-et-al_1986}
package \parencite{Dongarra-et-al_2008}. Overall, then, these
resources diverged both in scope and in target audience. Curiously, if
for different reasons, they all raised concerns of a ``black-box''
approach in the hands of users who had limited familiarity with the
underlying mathematical theory, or with relevant numerical techniques
\parencite{Shampine_1987}.

For all its positive features, the SLATEC library presented one major
shortcoming in the mid-1990s: it was written in Fortran. Fortran had
been the programming language of choice in science and engineering
since the late 1950s; beginning in the 1980s, however, it was
increasingly replaced with C. Therefore, GSL was framed explicitly as
``a modern version of SLATEC'' \parencite{Galassi-et-al-GSL-design}
(evidently, a reference to the SLATEC library, not the committee;
Section~\ref{sec:landscape}). Galassi and James Theiler began work on
design and early implementation of GSL, developing the first modules,
in 1996. The pair were soon joined by Brian Gough, who contributed to
both aspects of the project. Once the initial elements were in place,
it was possible to recruit several others to join in the effort. In
particular, Gough went on to co-lead with Gerard Jungman on overall
development of the library, including design and implementation of the
major modules \parencite{GSL-web}. All four were based at Los Alamos
National Laboratory: Galassi, Theiler, and Jungman as research
scientists, Gough working on development and modernization of the web
interface to the facility's preprint server (the predecessor to
\url{arXiv.org}). Multiple versions of the library were released
beginning in 1996, with the most recent stable release put out in
2019 \parencite{GSL-web}.

A crucial initial step in development of GSL was to outline an
explicit framework in a design document, which was refined over
several years \parencite{Galassi-et-al-GSL-design}. The document began
by identifying the need for a numerical library of high quality that
was free software (Section~\ref{sec:definitions}). The library would
include state-of-the-art algorithms, which would be described
pedagogically in comprehensive documentation. The code would be
written in C, using up-to-date coding conventions and standards, and
using a build and release system that would ensure portability and
robust configuration on different platforms. Overall, the library
would be aimed at general users, not specialists. At the same time, it
would provide a framework to which experts in numerical analysis would
be able to contribute.

These principles were conceived to build on the positive features of
related resources available at the time, while addressing various
shortcomings, as outlined above. In all other respects, the general
approach to the design of GSL was derived from established practices
of the free software movement, and of the GNU Project in particular
(Section~\ref{sec:history-1980s-free}) --- including release of the
library under the GPL (Section~\ref{sec:licences}), adoption of the
GNU coding standards \parencite{GNU-web-standards}, and use of the GNU
Autotools \parencite{Vaughan-et-al_2000} to manage the complexities of
porting software to different platforms \parencite{Galassi_2018}. As
stated in the design document, ``basically, [the library] is
GNUlitically correct'' \parencite{Galassi-et-al-GSL-design}. The GSL
team never lost sight of these principles, even as Galassi moved on
from the role of maintainer, succeeded first by Gough, and then by
Patrick Alken, a research scientist at the University of Colorado
Boulder.

\subsection{Lessons learnt}
\label{sec:lessons}

We can now review the outlook for the resources introduced in
Sections~\ref{sec:landscape} and \ref{sec:gsl-background}, separately
for different categories of software: software in the public domain
(Section~\ref{sec:lessons-public}), proprietary software distributed
commercially (Section~\ref{sec:lessons-proprietary}), and free
software distributed non-commercially
(Section~\ref{sec:lessons-free}).

\subsubsection{Public-domain software}
\label{sec:lessons-public}

Work authored as part of the duties of officers or employees of the US
government is not covered by copyright, therefore it is automatically
in the public domain (Section~\ref{sec:copyright}). At various times,
however, federal research laboratories have been run as
``government-owned, contractor-operated'' facilities, with personnel
employed by the contractor, not the government. Exploiting this
technicality, administrators of those facilities have often
``experimented'' with approaches to the commercialization of software,
variously restricting use, modification, and distribution of computer
programs developed in-house.

A misguided attempt at commercialization may explain why the
maintainers of the CLAMS library dithered when, in the early 1990s,
Galassi approached them about the source code
(Section~\ref{sec:landscape}). Whether through administrative
shortsightedness, or as an unintended consequence of it, in the end
the source code was not released, and the library was therefore never
ported to modern operating systems. A key resource for scientific
computing was thus lost through a bureaucracy that lacked a robust
framework for licencing software.

In and of itself, demise of the CLAMS library represents a waste of
the effort (intellectual, financial, administrative, \etc) that had
enabled development and maintenance of the codebase over several
years. There are also enduring repercussions for the research
community more broadly, in that it is not possible to reproduce
published results that relied on the library for calculations
(Section~\ref{sec:scene}).

Conception of the SLATEC library in the late 1970s predates such
attempts at commercialization. Nearly 40 years since its initial
release (in 1982, Version 1.0, written in Fortran 66), the library is
available to researchers worldwide as public-domain software. The
latest release (in 1993, Version 4.1, written in Fortran 77) features
over 1400 general-purpose mathematical and statistical
routines \parencite{Fong-et-al_1993}.

This outcome is not the fortuitous byproduct of an auspicious set of
bureaucratic circumstances. As discussed in
Section~\ref{sec:landscape}, development of the SLATEC library was
driven by awareness of the benefits that arise from the pooling of
technical expertise across research facilities
\parencite{Vandevender-Haskell_1982}. Accordingly, there was an
explicit focus from the outset on ``free software'' --- both in terms
of incorporating existing high-quality code into the library, and in
terms of making the programs available to all, encouraging uptake of
the library across the research community \parencite[304--306,
310]{Buzbee_1984}. For example, stringent standards were initially set
for programming, documentation, error handling, and testing, but they
were quickly reframed as recommendations, so as to facilitate
integration into the codebase of ``free software'' available from
other sources
\parencite[304--306, 314]{Buzbee_1984}. Additionally, new programs
considered for inclusion in the codebase were required to be in the
public domain, which was ``generally not a problem since most authors
are proud of their work and would like their routines to be used
widely'' \parencite{Fong-et-al_1993}.

The absence of restrictions on distribution of the software enabled
access via the network, practically as soon as the relevant technology
allowed. As noted in Section~\ref{sec:landscape}, in 1985 specific
packages of the library were included in the initial release of
Netlib, a repository that aggregated public-domain software for
scientific computing, to encourage sharing across the research
community \parencite{Dongarra-Grosse_1987, Dongarra-et-al_2008,
  Netlib-web}. The temporal overlap in development of the two
resources is likely not causal, and it is likely no coincidence that
both efforts involved scientists based at US government research
facilities --- possibly reflecting a culture that promoted the
(relatively) free circulation of knowledge, echoing the culture
established in the early days of military funding for computing
projects \parencite{Nofre-et-al_2014}.

We saw in Section~\ref{sec:history-1980s-free} that free software and
related concepts were not articulated in full until the late 1980s. In
particular, copyleft licencing had not been introduced at the time the
SLATEC library and Netlib were first released. Therefore, the
researchers involved in development of the two resources had no other
option to make the programs ``free software'', but to place the source
code in the public domain (Section~\ref{sec:licences}). There are
several advantages to this approach: anyone can obtain the programs at
no cost, and anyone is allowed to distribute copies of the source
code, with or without changes. It follows that anyone can make
improvements and contribute them back to the community, by
distributing modified versions of the programs as FOSS. However, there
is nothing to prevent distribution of the modified versions as
proprietary software instead.

Distributing proprietary derivatives amounts to ``free-riding'' on
collective effort \parencite[184]{Stallman_2015_ch-29}. Allowing
(enabling?)~such an uncooperative outcome is the one drawback to the
approach. In all other respects, the ``experiment'' set out in
establishment of the SLATEC library was a resounding success, likely
bolstered by confluence with the creation of Netlib
(Section~\ref{sec:landscape}). Both resources remain relevant
today. As of February 2021, Netlib reports over 1.24 billion
cumulative requests to its repositories at the University of Tennessee
and Oak Ridge National Laboratory \parencite{Netlib-requests-all},
with over 17.76 million of those requests being accesses to the SLATEC
library \parencite{Netlib-requests-SLATEC}. These figures relate to
only one of four main Netlib servers, and there are several mirrors
worldwide \parencite{Netlib-mirrors}. The email interface to Netlib
continues to operate, more than 35 years on, alongside the web-based
one \parencite{Dongarra-Grosse_1987, Dongarra-et-al_2008}.

\subsubsection{Commercial proprietary software}
\label{sec:lessons-proprietary}

There is a clear parallel between the spirit of collaboration that
culminated in release of the SLATEC library in 1982 --- and,
separately, in the launch of Netlib in 1985 --- and the culture of
knowledge-sharing that animated the free software movement in the
early 1980s (Section~\ref{sec:history-1980s-free}).

This sentiment was not universally shared across the research
community, however. For example, it is striking that those various
initiatives emerged at around the same time as publication of the
first edition of \citetitle{Press-et-al_1986}, in
\citeyear{Press-et-al_1986} \parencite{Press-et-al_1986}
(Section~\ref{sec:landscape}). In particular, the approach to
licencing taken by the authors of the book provides a jarring
contrast: as noted in Section~\ref{sec:landscape}, the commercial
proprietary licence imposes stringent restrictions on use and
repurpose of programs in the package, such that distributing software
that includes \citetitle{Press-et-al_1986} code is a violation of
copyright.

Whether intentionally or inadvertently, the licencing terms hindered
collaboration among researchers. By the time the second edition of the
book appeared, in
\citeyear{Press-et-al_1992} \parencite{Press-et-al_1992}, this stance
seemed untenable --- irksome even, since the authors acknowledged
public funding for related work. Ironically, they also acknowledged
relying on various free software tools for composition of the book
\parencite[xiii, xv]{Press-et-al_1992}.

Whatever gains (reputational, financial?)~were obtained in the short
to medium term, the approach to licencing eventually backfired,
exposing an embarrassing lack of foresight on the part of the authors.
High-quality free software for scientific computing had become
increasingly available through the 1990s and the early to mid-2000s,
and the \citetitle{Press-et-al_1986} codebase was simply no match for
it. The third edition of the book was published in
\citeyear{Press-et-al_2007} \parencite{Press-et-al_2007}; by admission
of the authors, printed code was now included in the text exclusively
as a pedagogical tool \parencite[xi]{Press-et-al_2007}. More
generally, the package was relegated to a relatively minor role
against its earlier popularity (Section~\ref{sec:landscape}),
occupying a narrow niche in the flourishing landscape of scientific
computing.

Overall, then, the outlook for the \citetitle{Press-et-al_1986}
codebase is analogous to the outlook for the CLAMS library
(Section~\ref{sec:lessons-public}): a waste of the disparate set of
resources that had enabled the work, with repercussions for the
research community at large in terms of reproducibility
(Section~\ref{sec:scene}).

\subsubsection{Non-commercial free software}
\label{sec:lessons-free}

We can only speculate what alternative fate the
\citetitle{Press-et-al_1986} package would have enjoyed, had its
authors not restricted use of the programs, nor prohibited their
distribution --- perhaps even inviting users, and the broader
community, to improve the software collaboratively.

To this end, we can draw a comparison with GSL, a project that began
between publication of the second and third editions of the book. The
library is in active development 25 years since its initial release
(in 1996, Version 0.0). The most recent stable release (in 2019,
Version 2.6) includes over 1000 mathematical routines, together with
an extensive test suite
\parencite{GSL-web}. Current efforts focus on maintaining the
stability of the codebase, on extending or improving existing
functionality, and on introducing new capabilities by incorporating
useful algorithms that have been independently tested and thoroughly
documented. Wrappers for the library are available from C to several
high-level programming languages. Finally, GSL is readily installed
from source code, and it is ported to different platforms efficiently
and robustly --- including the major GNU/Linux packaging approaches
(Debian, Red Hat) and proprietary operating systems (macOS, Windows).

Where possible, the routines included in GSL were based on
public-domain software of high quality, reimplemented in C using
up-to-date coding conventions and standards (\eg name
canonicalization); others were instead written from scratch, following
the same approach in terms of implementation \parencite{GSL-web}. The
stability over time of C coding conventions and standards has lent
robustness to GSL. Additional robustness has grown out of the explicit
design decision to adopt rigorous engineering practices linked to the
GNU Project (Section~\ref{sec:gsl-background}). For example, use of
the GNU Autotools \parencite{Vaughan-et-al_2000} as the build system
ensures longevity alongside portability. This suite of tools generates
source code distributions that can be run virtually unchanged decades
after they are released, through the rigid application of standards
(\eg use of the POSIX shell instead of GNU Bash for shell scripts),
and through the assumption of minimal features on the target
system. As a result, it is still possible to build the earliest
versions of GSL, 25 years on, with almost no
changes \parencite{Galassi_2018}.

Another example of best practice derived from the GNU Project is
inclusion in GSL of a test suite for all routines --- for instance, a
program that uses the library to check the output of a routine against
known results, or one that invokes the library several times and
performs a statistical analysis on the results (\eg for random number
generators). The test suite is run automatically as part of the
building of the library \parencite{GSL-web}. Such complete testing was
a new feature in the late 1990s, preceding widespread uptake of the
practice in industry and, eventually, in academic research.

As discussed in Section~\ref{sec:gsl-background}, GSL was conceived as
a modern replacement to the SLATEC library. It was also intended as an
alternative to a host of other resources for numerical analysis
available in the 1990s, which included both free and proprietary
software, and both commercial and non-commercial packages
\parencite{GSL-web}. The advantages to using a tool like GSL that is
free software, distributed non-commercially, are as follows. First,
the library is available to everyone, at no cost, and therefore
without conditions on in-house use (\eg a limit on the number of
users, which commonly applies to commercial products). Second,
availability of the source code means that users can customize the
routines in GSL to their needs. Third, users are allowed to release,
as source code, any software they develop that is derivative of GSL
--- including software that makes use of the library, and software
that incorporates a non-trivial amount of code from it. Overall,
then, GSL enables and promotes scientific
collaboration \parencite{GSL-web}.

One restriction that arises from licencing of GSL under the GPL is
that the library can only be redistributed in software that is itself
under the GPL (Section~\ref{sec:licences}). For example, users are
allowed to combine GSL code with a program under a different licence,
if the licence is compatible with the GPL. They are also allowed to
distribute the combined program, including the source code, provided
that the program is released under the GPL. It follows that all
derivatives of GSL grant users the same rights that are granted by
GSL \parencite{GSL-web}. By design, then, the freedom built into GSL
by way of the GPL begets more freedom (Section~\ref{sec:definitions}),
in the sense that it gets passed on to any software that relates to
the library, in perpetuity (Section~\ref{sec:copyright}). In practice,
a positive side-effect is that improvements to GSL are automatically
contributed back to the community, to the extent that modified
versions of routines in the library can only be distributed as
FOSS. Therefore, copyleft licencing averts the free-riding on
collective effort enabled by FOSS that allows proprietary derivatives
(Section~\ref{sec:lessons-public}) --- namely, FOSS in the public
domain, and FOSS under a non-copyleft licence
(Section~\ref{sec:licences}).

The restriction on distribution imposed by the GPL effectively serves
to prevent the imposition of further restrictions
(Section~\ref{sec:licences}), and it applies equally to all users,
whether they distribute software commercially or non-commercially
(Section~\ref{sec:misconceptions-free}). In the case of GSL, it is the
``price'' for access to a high-quality numerical library at no
cost \parencite[1--2]{Galassi-et-al_2009}. Opinions differ as to
whether such a restriction should extend to software developed in the
context of research. For example, a recent recommendation is to avoid
the GPL in scientific computing, and to use a permissive licence
instead (\ie a non-copyleft licence; Section~\ref{sec:licences}). The
rationale is that permissively-licenced FOSS is more easily integrated
into other projects \parencite{Wilson-et-al_2017}. Another view is
that where the overarching aim is knowledge production and
dissemination, as in the context of research
(Section~\ref{sec:scene}), then convenience should give way to
principle --- in this case, the principle of software
freedom \parencite[235]{Stallman_2015_ch-39}.

In setting up GSL, Galassi and colleagues were able to draw on the
licencing philosophy that had emerged from the free software movement
in the late 1980s (Section~\ref{sec:history-1980s-free}). As discussed
in Section~\ref{sec:lessons-public}, such a carefully thought-out
approach to the distribution of software had not been available to the
researchers involved in establishment of the SLATEC library in the
late 1970s, and in the creation of Netlib in the early 1980s. In many
other respects, the parallels between the SLATEC library and Netlib
outlined in Section~\ref{sec:lessons-public} extend to GSL. GSL also
sought to prevent duplication of effort, both by re-using existing
programs, and by enabling the research community to share software ---
not reinventing the wheel, so to speak, being a recurrent theme in the
free software tradition (Section~\ref{sec:history-1980s}). And, as had
been the case for the SLATEC library and for Netlib, development of
GSL was driven by scientists based at US government research
facilities (Section~\ref{sec:gsl-background}). This observation raises
questions about the incentives that motivated those involved in the
three projects, compared to the incentives that may prevail in other
contexts. For example, contemporary university settings have long
suffered from an emphasis on ``publish or
perish'' \parencite{Willinsky_2005}, which leads to framing the
research process as a ``winner-takes-all'' activity; in turn, such
framing rewards short-termism and performative novelty, at the expense
of research quality and sustainability.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
