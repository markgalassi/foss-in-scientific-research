%%% Local Variables:
%%% TeX-master: "foss-in-scientific-research"
%%% End:

\section{Prolegomena for future research software}
\label{sec:prolegomena-for-future-research-software}
\subsection{Existing exhortations}

Several useful articles have been written with practical tips to
improve a scientific software pipeline.  Some, such as
\autocite{wilson2014best,Jimenez-et-al_2017}, give general
prescriptions.  Others, such as
\autocite{mccarty2008physicsSoftwareRant,callaway2009pointsOfFail},
focus on the importance of making the software friendly to packagers
and other recipients of the software.  Barba's Reproducibility
Manifesto \autocite{Barba2012} states the firm intention to follow a
set of guidelines which are well chosen, and in turn points to another
manifesto: the Science Code Manifesto
\autocite{barnes2020scienceCodeManifesto}.

These articles and manifestos give recommendations for a workflow
which offers a great improvement over the traditional picture: the
scientist who writes one-time programs, runs them, pastes the
resulting plots into a word processor, and then loses track of the
original software and data files as soon as the paper is published.

Organizations such as Software Carpentry have sprung up to teach
scientists how to use version control, pipelines, shell scripts,
programming in Python and R, build systems, test suites, \dots Thanks
to methodical organization and wide reach, Software carpentry's
courses have raised the bar significantly: at this time a good
fraction of scientific software meant to be used by others is written
to a much higher standard \autocite{jordan_kari_l_2020_3753528}.


\subsection{Longevity and the need for greater rigour}

The aforementioned workflow recommendations often emphasize user
friendly tools which are available to set up such a workflow, such as
the user interface of github, the use of python interactive notebooks,
and programming with integrated development environments.

One problem with this approach is that scientists can lose control of
their workflow.  Apart from not being automated, maybe the most
worrisome aspects of these approaches is the entrance of proprietary
elements into the procedure: for example the use of the features of
github that go beyond just having a public git repository.  If
proprietary web features are used to build and run the software and
produce ``artifacts'', then we do not have a reproducible research
product.

Other limitations come from the graphical tools that are part of the
currently trendy solutions.  Software is usually used for years or
decades longer than originally intended\footnote{One of the ``lessons
  learned'' from the \$300 billion to \$600 billion spent on the ``Y2K
  bug'' in the late 1990s is that software lasts much longer than it
  should.  The rule of thumb is to assume a lifetime of no less than
  17 years for a program you write.}, and its use will likely outlast
the graphical tools that were used to create it.  In addition,
graphical tools seldom generate reproducible steps.

Longevity is also not addressed in most of these prescriptions.  The
wide use of old scientific packages, such as the GNU Scientific
Library and the even older Netlib, shows that longevity is a key
portion of the reproducible research picture.


\subsection{Examples of rigour: build systems, file formats, metadata,
  V\&V}

% The GNU project encourages the use of the GNU autotools (autoconf,
% automake, \dots), which generate source code distributions which can
% be run virtually unchanged decades after they are released.  This is
% due to
% \begin{inparaenum}[a)]
% \item emphasis on portability,
% \item the rigid use of standards (for example the POSIX shell instead
%   of bash for shell scripts), and
% \item the assumption of minimal features on the target system.
% \end{inparaenum}
% The success of the GNU project's approach can be seen in how long very
% complex programs such as GNU Emacs (35 years old), the GNU Compiler
% Collection (33 years old), and the GNU Scientific Library (24 years
% old) are still built with the same tools, and still follow the same
% coding standards.  The linking of build systems with version control
% tagging is key to release engineering, and forms the basis for the
% most rigorous type of reproducibility \autocite{galassi2018self}.

Other examples of robust thinking about longevity are Project
Gutenberg \autocite{hart1992history}, where the focus is on plain
ASCII text storage, and the HDF5 data format.  HDF5 is aimed at
sophisticated and high-performance data storage: a challenge too
complex to be solved by the use of ASCII storage.  Given this extra
complexity, to guarantee longevity there is a funded non-profit
consortium whose purpose is to steer the future of the hdf5 format and
library interfaces \autocite{folk2011overview}.

And we will recommend: \emph{always provide metadata in the file
  itself}.  Using the example from \autocite{galassi2020lem}, you
might record the height of your child Susan as a function of time like
this in a file called \texttt{heights.txt}:

\begin{verbatim}
0  50
1  74
2  86.5
3  95
4  102.5
5  109.5
[...]
\end{verbatim}

\emph{You} will personally know that column 0 has age and column 1 has
the height measured in centimeters, and that the data refers to your
child Susan.  But if this file ends up in someone else's hands, the
descriptive information might have gotten lost.

\emph{Never create such a file!  Even for just a few minutes.}
Anything you add to those numbers will help a lot.  We recommend that:
if you have a very simple file that is best kept as plain text, you
should insert what we call ``low effort metadata'' (LEM).  These are
structured comments at the start of the file that give you information
about it.

Here is something that is already much better than the example above:

\begin{verbatim}
##COMMENT: Height (cm) as a function of age (years) for Susan
0  50
1  74
2  86.5
3  95
4  102.5
5  109.5
[...]
\end{verbatim}

That last example was human-readable, but not easily machine-readable.
An improvement is to make it so that a program can easily pick out the
metadata:

\begin{verbatim}
##COMMENT: Height as a function of age for Susan, born 2009
##LAST_MODIFIED: 2020-05-12
##FILENAME: heights.txt
##COLUMN0: age
##COLUMN0_UNITS: years
##COLUMN1: height
##COLUMN1_UNITS: cm
0  50
1  74
2  86.5
3  95
4  102.5
5  109.5
[...]
\end{verbatim}

Note that LEM does not follow a rigid standard for what the fields
should be called: you pick what is important.  This allows you to
insert the metadata without it being ``a thing'' that causes you
stress and might discourage you from doing it.

As a final example of rigour we mention formal verification and
validation (V\&V).  When dealing with very expensive hardware or even
human life, funding agencies can require that an independent research
group examine scientific software for purpose of verification (whether
the model is solving the equations correctly) and validation (whether
they are the correct equations for your
purpose). \autocite{roache1998verification}

The U.S. departments of Defense and Energy often require V\&V of
significant scientific endeavors
\autocite{navy2005ModelingAndSimStandard}.  An example is the RAND
corporation's report on the readiness of the US Department of Defense
in AI research, and it is interesting to note their emphasis on use of
open source components and version control repositories
\autocite[p.~57]{tarraf2019department}.  Strongly written reports of this
sort go deeper and have more authority than most journal peer review.


\subsection{The prescription}
\label{subsec:the-prescription}

Our thinking about robust longevity of research software, and about
the careful use of a FOSS pipeline, informs our prolegomena.  These
points supplement the prescriptions given in
\autocite{Jimenez-et-al_2017,wilson2014best,mccarty2008physicsSoftwareRant}
in some important ways.

We separate them into \emph{strategic} and \emph{tactical}
prescriptions.  The former lead to broad and long-term advantages for
the software, while the latter are techniques and principles that help
implement the strategic prescriptions.

\begin{description}
\item[Strategic]\hfill
  \begin{enumerate}
  \item Identify which pieces of software will live for a long time,
    and apply these prescriptions to those programs.  You should be
    very generous in assuming your software will last long: most
    scientists take their tiny one-off scripts and use them for the
    rest of their career, often expanding them to become large
    programs.  Assume your software will last 17 years unless you can
    really convince yourself otherwise.
  \item Only rely on free and open source (FOSS) software.
  \item Have your research result be reproducible by running a single
    unattended shell script.  And have that be the way \emph{you
      yourself} generate your output.
  \item When selecting software, develop skepticism about slogans,
    especially industry slogans.  Only use terms that have clear
    definitions. When you see the words ``open'' and ``standard'', for
    example, look at what is really guaranteed.
  \item Use a serious programming language to process your data or
    carry out simulations.  Do not use a spreadsheet.  But don't get
    too carried away with the latest programming language \emph{du
      jour:} use Python for most things, R if all your community only
    knows R, C/C++ (and possibly their successors like Go and Rust)
    for very high speed work and embedded systems.  (These are
    language choices for the year 2020 -- they might slowly evolve.)
  \item Have a build system for your software.  Autotools and CMake
    have shown good staying power and are still good choices, but they
    are now both old and it might be worth investigating successors.
  \item Pick file formats that will last a long time, like ascii text
    and HDF5.
  \item Understand the philosophy of UNIX pipelines, and develop your
    processing pipeline in that spirit.
  \end{enumerate}

\item[Tactical]\hfill
  \begin{enumerate}
  \item Text, text, text.  Favor tools which save the state of your
    work as plain text.  Write programs that save machine-readable
    plain text.
  \item Use a FOSS licence.  Don't try to craft it cleverly: just use
    the GPL, unless you have very compelling and specific reasons to
    do
    otherwise.\cite{mccarty2008physicsSoftwareRant,callaway2009pointsOfFail}
    Remember: just the fact that you put your source code somewhere
    does not make it FOSS.  You have to make it explicit, for example
    with a LICENSE file at the top level directory.
  \item Use metadata for your data and output files!  If you don't
    have time to plan a fancy metadata architecture, just write out
    Low Effort Metadata (LEM).
  \item Save data files in plain text formats if they are small, and
    in hdf5 format if they are large or require sophisticated
    metadata.
  \item Generate output files from input files using a pipeline that
    considers modification timestamp or a checksum on the file
    contents.  The UNIX ``make'' utility can be used to automate
    timestamp dependency.
  \item Confine your use of graphical programs to where they are truly
    needed.  For example: draw logos with GIMP or Inkscape, but do not
    manipulate your plots with interactive graphical programs: they
    will not be reproducible.
  \item If you are really enamoured with a graphical program for a
    task, make sure that it produces ascii text which can be run in a
    pipeline, and that the program \emph{uses that saved text file as
      its authoritative format for reloading its state}.
  \item Have scripts insert figures, tables, and specific output
    numbers into your papers directly, with no human intervention.
  \item Follow a set of coding standards.  If you don't have one fall
    back on the GNU coding standards.
  \item Write test cases appropriate for your software.
  \item Link your version control tags to versions of your software,
    data, and writings.  This propels you into the highest level of
    software reproducibility.
  \item If you use a version control hosting site, restist the
    temptation to use features other than version control.  These
    features will lock you in to that hosting site, and some might
    interfere with reproducbility.  They are also often based on
    proprietary infrastructure.
  \end{enumerate}
\end{description}
