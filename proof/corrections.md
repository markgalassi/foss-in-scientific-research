p. 18: change to "amounts to 'free-riding' on" --- i.e. add single quotation marks

[ref. 44]: change to "Open Source Initiative. *Licences*" --- proof has "Open Source. Initiative *Licences"*

[ref. 81]: change to "(ed WR Cowell)" --- compare e.g. ref. 61, which has "eds"

[ref. 90]: change to "Shampine LF. 1987 *Numerical Recipes, The Art of Scientific Computing*. By W. H. Press, B. P. Flannery, S. A. Teukolsky, W. T. V. Vetterling. Cambridge University Press, 1986. xi + 817 pp. *Am. Math. Mon.* **94**, 889–892. ([doi:10.1080/00029890.1987.12000737](https://doi.org/10.1080/00029890.1987.12000737))" --- format exactly as specified here, i.e. italicise book title that is part of article title; italicise journal name and make volume bold