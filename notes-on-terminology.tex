%%% Local Variables:
%%% TeX-master: "foss-in-scientific-research"
%%% End:

\section{Notes on terminology}
\label{app-notes-on-terminology}

\subsection{Kernels and operating systems and GNU}

\begin{description}

\item[kernel versus operating system] One of the classic ambiguities
  arises with the term ``operating system'': it is used to refer to
  two different things.  For some the operating system is only the
  ``kernel'': that software layer which talks directly to a computer's
  hardware and offers an abstraction for application programs to use
  the hardware.  For others the operating system is the kernel plus
  the collection of libraries and programs which makes for a fully
  usable computer.  The Debian GNU/Linux operating system, for
  example, provides fifty one thousand software packages of free
  software.  We will use the latter meaning for ``operating system'':
  the kernel plus the collection of programs shipped by the system
  provider.

\item[Linux versus GNU/Linux] Much argument surrounds the use of the
  term Linux versus the term GNU/Linux: some use one, some the other,
  to refer to a full operating system based on the Linux kernel.  We
  will use the term Linux to refer to the kernel, and GNU/Linux to
  refer to a full operating system based on the Linux kernel.
\end{description}


\subsection{The perils of the words ``open'' and ``standard''}
\label{subsec:the-perils-of-the-words-open-and-standard}

In the 1980s and 1990s, before the term open source was coined, and
before many software executives were familiar with notion of
\emph{free as in freedom}, some industry organiztions with the word
\emph{open} sprouted, to try to standardize UNIX efforts.

The Open Software Foundation was a consortium between seven
proprietary UNIX vendors aimed at promoting common standards for UNIX
systems.  Their most visible product was the OSF/Motif widget set for
the X window system.  First put out in 1989, Motif became irrelevant
with the advent of competing window systems from other proprietary
vendors.  It was only released as free software in 2012.

It feels ironic to use the word ``open'' to refer to proprietary
software which was locked up and useless for decades, but more
importantly it provides an early example that leads to a warning:
always question overloaded words that engender a feeling but have no
clear referent.  The expressions ``free software'' and ``open source
software'' have precise definitions and can be the basis for an
edifice of thought, but most uses of the single word ``open'' should
be seen as conveying a feeling (sometimes for marketing purposes) but
not forming a concrete basis for understanding a researcher's most
important tool.

The term ``open access'', used to refer to freely available and
distributable publications, might be an exception to this tendency to
bandy the word open imprecisely: the Budapest Open Access Initiative
(BOAI) defined open access \autocite{budapest2012ten}, and maintains a
web page with its definition and principles.  Most importantly they
have made a strong recommendation for the use of the Creative Commons
CC-BY license.  Unfortunately there has been a proliferation of
definitions and \emph{de-facto} definitions which could even dilute
the CC-BY licensing requirement.  These issues are discussed well in
\autocite{anderson2017diversity}.

The word ``standard'' is also often used incorrectly.  You sometimes
hear a manager say ``we are standardizing on Microsoft Internet
Explorer 7'', or make reference to some company's product as if it
were a standard.

There are national standards bodies (ANSI in the US, for example) and
international ones as well (the ISO).  There are industry consortia,
non-profits, even sometimes single companies, which publish standards
in which they make a \emph{promise} to anyone who adopts that
behavior.  They do not promise that a program will behave in a certain
manner: they promise that the saved file formats will be maintained
carefully and transparently.  These are usually not flippant gestures,
and can be used as a basis for software strategy.
