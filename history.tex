\subsection{Historical framing}
\label{sec:history}

In Section~\ref{sec:definitions} we introduced two social movements
active in the FOSS community. Here we frame the two in historical
context, as background to concepts discussed in
Sections~\ref{sec:name} and \ref{sec:legal}, and to the case study we
present in Section~\ref{sec:gsl}. Our account is necessarily
incomplete, focusing on key developments directly relevant to the
material covered in those sections. We point readers
to \parencite[3--7]{Fogel_2017} for a useful overview of the history
of FOSS, and to \parencite{Salus_1994, Raymond_2001, Moody_2002,
  Williams_2002, Ceruzzi_2003, Levy_2010, Coleman_2013,
  Kernighan_2020} and various chapters
in \parencite{DiBona-et-al_1999} for detailed treatments of specific
topics.

\subsubsection{Origins and key developments through the early 1980s}

The origins of today's FOSS culture can be traced back to the
tradition of openness, collaboration, and knowledge-sharing that
characterized the early decades of computing, sometimes referred to as
``Hacker Ethic'' \parencite[ch.~2]{Levy_2010}. In
Section~\ref{sec:plan} we pinpointed to the early 1970s the emerging
tension between the hacker tradition and the commercial interests of a
nascent software industry. Rather than a specific event, this window
captures several significant interrelated changes that occurred around
that time.

\paragraph{The ``unbundling''}

One such change was the ``unbundling'', \ie separation of the sale of
software and services from the sale of hardware, starting in 1969 with
IBM \parencite[106--107]{Ceruzzi_2003}. Up until that point, corporate
profits had been driven by manufacture and marketing of
hardware. Therefore, the cost of software and services had been
combined, or ``bundled'', with the cost of hardware. Each hardware
model was highly specialized, as consequently was the software written
for it. As a result, manufacturers had little incentive to sell
software separately from hardware. They also had no incentive to
prevent software from being shared widely. In fact, the widespread
distribution of programs for a specific model often led to
user-contributed updates, which improved sale prospects for that
model \parencite[5]{Brasseur_2018}. This dynamic affected early
corporate motivations for investment in software development. It also
affected how companies distributed source code, and the relationship
they kept with volunteer developers (\eg users in academia operating
the hardware, typically researchers and technicians).

The unbundling decision by IBM was precipitated by the threat of an
antitrust lawsuit by the US government, on the charge that the
bundling of hardware and software was anticompetitive. More broadly,
the decision captured an inevitable transition in the computer
industry \parencite[169]{Ceruzzi_2003}. Hardware manufacturers had
begun to realize that software development required substantial
effort, and that it held potential as a revenue stream in and of
itself \parencite[4]{Brasseur_2018}. For example, the introduction of
high-level programming languages meant that software could be ported
to different hardware models, creating the conditions for a commercial
software industry separate from the sale of
hardware \parencite[3]{Fogel_2017}. Beginning with the unbundling by
IBM in 1969, software effectively became merchandise.

\paragraph{Rise of Unix}
\label{sec:history-origins-unix}

That same year saw another key development, namely creation of the
Unix operating system \parencite{Ritchie-Thompson_1974}. It is no
exaggeration to say that Unix provides the foundation to the world's
digital infrastructure \parencite[158, 165]{Kernighan_2020}. Today,
the majority of web servers and the top 500 supercomputers run
operating systems based on the Unix design. Unbeknown to many,
practically every ``smart'' device we interact with on a daily basis
also runs some variant of Unix --- including our phones, computers,
routers, headsets, and so on. For example, both Android and iOS are
variants of Unix, as are the GNU/Linux operating system in all its
flavours (\eg Debian, Ubuntu), Chromium/Chrome OS, and macOS.

\subparagraph{Development at Bell Labs}

Unix was created beginning in 1969 by Ken Thompson, Dennis Ritchie,
and other researchers at Bell Telephone Laboratories, ``Bell Labs''
for short. Bell Labs was a division of AT\&T, the American Telephone
and Telegraph Company; at the time, it was co-owned with Western
Electric, which was itself a subsidiary of
AT\&T \parencite[57]{Salus_1994}.

Unix was initially released for internal use at Bell Labs. From the
early 1970s it was licenced to educational institutions for a nominal
``media fee'', and to a limited number of corporate costumers for a
commercial fee \parencite[132, 143]{Kernighan_2020}. For legal
reasons, the company was prohibited from turning the system into a
product and marketing it \parencite[57--61]{Salus_1994}. Therefore,
the licences included all of the source code, but on an ``as is''
basis \parencite[134--135]{Kernighan_2020}. Furthermore, there were
restrictions in place: for example, licencees in universities could
use the system exclusively for educational purposes
\parencite[132]{Kernighan_2020}.

The earliest versions of Unix were written in assembly language (\ie
the machine-dependent language for the specific hardware model
available to the researchers at Bell Labs). Relatively early on, most
of it was rewritten in C, a high-level programming language created by
Ritchie at around the same time \parencite[76--78]{Kernighan_2020}. As
a result, Unix was the first operating system to be ported to a wide
variety of platforms \parencite[140--141]{Kernighan_2020}.

\subparagraph{Development at Berkeley}

The system's portability allowed the source code to be easily adapted
to different machines, including the low-end models often available in
university departments. Coupled with the inexpensive licencing, this
feature led to widespread circulation of Unix in academia, starting in
the early 1970s \parencite[134]{Kernighan_2020}. In turn, uptake of
the system in this setting contributed to an upward spiral of
technical innovation. For example, from the mid-1970s a group of
researchers at the University of California, Berkeley developed a
version of Unix with expanded scope and
capabilities \parencite[31--33]{McKusick_1999}. This version was
released beginning in the late 1970s as the Berkeley Software
Distribution (BSD) under a permissive licence
(Section~\ref{sec:licences}).

Unix was thus adopted as an educational tool across institutions in
several countries \parencite[\eg][]{Lions_1996}: its relative
simplicity was well suited to demonstrating the implementation of a
real-world operating system. As computer science graduates ventured
from university into industry, they contributed to the growing
popularity of the system outside
academia \parencite[144--145]{Kernighan_2020}.

\subparagraph{Commercialization}

Unix development was run through the 1970s ``as a loosely proprietary
research project at AT\&T'' \parencite[5]{Fogel_2017}. Given that the
company did not provide any support, users started coming together on
a regular basis to share ideas and
software \parencite[65]{Salus_1994}. Improvements were fed back to the
researchers at Bell Labs for integration into later releases, and this
group acted as an informal ``clearing house'' for exchange of the
system in the US.

By the late 1970s Unix had accrued thousands of users. The surge in
popularity led AT\&T to cease distribution on the previous terms, and
to begin enforcing its copyright on the source code. This shift in
posturing was followed by a change in the company's legal position in
the early 1980s, which lifted the restrictions that had prevented the
full commercialization of the system until
then \parencite[143--145]{Kernighan_2020}.

Unix was thus promptly taken to market. The new licencing terms were
not as favourable as the previous ones had been, so the Berkeley
researchers continued to develop and distribute the BSD version as an
alternative to the commercial products available from
AT\&T \parencite[154--155]{Kernighan_2020}. The researchers at Bell
Labs were no longer able to act as a clearing house, even informally,
and this role was taken up by their peers at
Berkeley \parencite[34--35]{McKusick_1999}.

\subsubsection{Key developments in the 1980s}
\label{sec:history-1980s}

\paragraph{The free software movement}
\label{sec:history-1980s-free}

By the time Unix was taken to market, the commercial software industry
had converged on a clear position in relation to the distribution of
source code. Companies needed to produce reliable products in order to
gain an edge over the competition \parencite[108]{Ceruzzi_2003}. To
this end, they needed to stem the unrestricted sharing of software
that had prevailed in the early decades of computing. For example,
they would now require users to sign non-disclosure agreements on the
code, or they would simply not distribute
it \parencite[3]{Fogel_2017}.

The changes occurring in industry spawned lucrative employment
prospects for programmers. They also led to frustration, however,
arising from the cost of acquiring software and, more importantly,
from no longer being able to customize programs and then share the
modified, improved versions \parencite[5]{Brasseur_2018}. One person
particularly aggrieved by the situation was Richard Stallman, a
programmer on staff at the AI Lab, the Artificial Intelligence
Laboratory at the Massachusetts Institute of Technology
(MIT). Stallman had thrived in the hacker culture of the lab through
the 1970s, but this community had progressively disbanded over the
years, as many of its members joined companies to work on proprietary
software.

As the changes that were underway in industry finally caught up with
MIT in the early 1980s, the situation became intolerable for
Stallman. Resenting the growing restrictions on both himself and
others, he resolved to create GNU, a Unix-compatible system that was
free software (Section~\ref{sec:definitions}). ``GNU'' is a recursive
acronym for ``Gnu's Not Unix'', indicating that the system would be
Unix-like in design, but that it would not include any Unix code.

Stallman announced this plan in a posting to two Usenet newsgroups
(\texttt{net.unix-wizards} and \texttt{net.usoft}) in 1983
\parencite{Stallman_2015_ch-03}. He then resigned from the AI Lab, to
prevent MIT from interfering with release of GNU as free software, and
in 1984 he began work on what would later become known as ``GNU
Project'' \parencite{GNU-web, Stallman_1999}. The project would
produce a complete operating system from scratch, including (i) a
kernel (\ie the program that allocates resources and interfaces with
the hardware), and (ii) a full collection of developer and user-level
utilities, to allow both development and general use. The aim was for
Stallman to ``be able to get along without any software that is not
free'' \parencite[27]{Stallman_2015_ch-03}.

Opening with ``Free Unix!'', the Usenet posting invited
``[c]ontributions of time, money, programs and
equipment'' \parencite[26]{Stallman_2015_ch-03}. This ``call to arms''
marks the birth of the free software movement
(Section~\ref{sec:definitions}). In 1985 Stallman issued the GNU
Manifesto \parencite{Stallman_1985, GNU-web-manifesto}, in which he
expanded on the initial announcement, with more detail on the
project's philosophical underpinnings. For example, he explained that
GNU software is not in the public domain, to ensure that all versions
remain free in perpetuity (Section~\ref{sec:licences}). That same year
he established the Free Software Foundation to support the movement,
and in 1986 he published an early version of the Free Software
Definition \parencite{Stallman_1986}
(Section~\ref{sec:definitions}). He then introduced the notion of
copyleft (Section~\ref{sec:copyright}), through release of the first
version of the GPL in 1989 (Section~\ref{sec:licences}). Effectively,
several key concepts outlined in Sections~\ref{sec:name} and
\ref{sec:legal} were first explicitly articulated by Stallman in the
1980s, and they delineate FOSS as we understand it today.

\paragraph{Demise of Unix}
\label{sec:history-1980s-unix}

In many ways, the free software movement resembled the community of
Unix users established in the 1970s. Together with the researchers
working on the system at Bell Labs and at Berkeley, this community had
been putting into practice the culture of knowledge-sharing that would
drive Stallman's ideological campaign beginning in the
1980s \parencite[5]{Fogel_2017}. What had been missing in the early
phases of Unix development, however, was a clearly stated philosophy
around software licencing (Section~\ref{sec:licences}), and the system
suffered uncertainty and loss of opportunity as a
result \parencite[157]{Kernighan_2020}.

One issue was the proliferation of Unix and Unix-like variants,
following commercialization of the system by AT\&T in the early
1980s \parencite[153--157]{Kernighan_2020}. A multitude of
implementations appeared, some based on versions available from AT\&T,
some based on BSD, some based on combinations of the
two \parencite[209--210]{Salus_1994}. In particular, the licencing of
BSD allows proprietary derivatives (Section~\ref{sec:licences}), and
BSD code was thus often incorporated into proprietary variants
(notably, for example, Apple's macOS and iOS are proprietary operating
systems based on derivatives of BSD). Such proliferation dispersed
effort, creating competition, fragmentation, and
incompatibility. Indeed, one of the benefits for ``all computer
users'' that Stallman envisioned from releasing the GNU system as free
software was precisely to avoid ``much wasteful duplication of system
programming effort'' --- effort that could be directed ``instead into
advancing the state of the art'' \parencite{GNU-web-manifesto}.

\subsubsection{Key developments in the 1990s}

\paragraph{Enter Linux}
\label{sec:history-1990s-linux}

The GNU system was nearly complete by the early 1990s, comprising a
large collection of packages. However, the design that had been
selected for the kernel proved particularly difficult to
implement. Therefore, development of this component lagged
behind \parencite[4]{Fogel_2017}.

The system was made operational through inclusion of a kernel based on
a less ambitious design. This kernel, called ``Linux'', was created by
Linus Torvalds beginning in
1991 \parencite[103--104]{Torvalds_1999}. Torvalds, then a computer
science student at the University of Helsinki, had started the project
as a hobby, but he managed to produce a working kernel from scratch in
a relatively short period of time. In part, he was able to do so with
contributions by developers around the world, leveraging the growing
capability of the Internet, which was getting off the ground in those
years \parencite[3]{Fogel_2017}.

Linux was released as free software under the GPL from 1992
(Section~\ref{sec:licences}). At this point, work began to integrate
the kernel with GNU software, so as to produce a fully functional
operating system \parencite[107]{Torvalds_1999} --- technically, this
is the GNU/Linux operating system, although it is commonly referred to
simply as ``Linux'' (see discussion in \parencite{Stallman_2015_ch-11,
  Stallman_2015_ch-12}).

\paragraph{Legal troubles}
\label{sec:history-1990s-unix}

Had a kernel been available as part of the BSD version, Torvalds would
likely not have bothered with his hobby project in the first
place \parencite{Linksvayer_1993}. However, development of BSD had
been slowed down in the early 1990s by legal turmoil around licencing
\parencite[157]{Kernighan_2020}.

Early versions of BSD included Unix code, and they were therefore
subject to an AT\&T licence. After AT\&T began clamping down on
distribution of the system, the researchers at Berkeley put
substantial effort towards reimplementation of various utilities which
used Unix code, so that BSD and its derivatives would no longer be
subject to the AT\&T licencing
requirement \parencite[40--43]{McKusick_1999}. Despite this effort, a
legal quagmire ensued between 1992 and 1994 over intellectual property
rights to the software. In particular, a subsidiary of AT\&T accused
BSD developers at Berkeley and elsewhere of violating the copyright on
Unix; the University of California responded with a counterclaim,
accusing AT\&T of breaching the terms of the BSD licence in using code
developed at Berkeley \parencite[44--45]{McKusick_1999}.

The legal troubles resolved largely in favour of the BSD developers,
and work on the project continued at Berkeley through the
mid-1990s \parencite[45--46]{McKusick_1999}. Yet by the time the legal
ambiguity had cleared, the market had shifted to GNU/Linux as the
leading Unix-like operating system that was free software, with BSD
and its derivatives relegated to relatively minor roles.

\paragraph{The open source movement}
\label{sec:history-1990s-open}

By the late 1990s, GNU/Linux had also gained large market shares as an
alternative to proprietary operating systems, notably Windows, both
among individual users and in business. Free software quickly gained
momentum from then on, in industry as well as outside the developer
community. Some in the community felt that this turning point called
for a ``rebranding'', so as to make free software palatable to
mainstream corporate types. A sticking point was the ambiguity arising
from use of the term ``free'' in relation to software
(Section~\ref{sec:misconceptions-free}). In particular, the need to
explain the concept with reference to ``free speech'' \vs ``free
beer'' diverted attention from the core issue of source code
availability \parencite{Peterson_2018, OSI-web-history}. Besides being
confusing to newcomers, the term failed to convey the possible
commercial advantages arising from software
freedom \parencite{Peterson_2018}, and in particular the greater
levels of innovation enabled by an open development model
(Section~\ref{sec:misconceptions-open}).

Brainstorming on the topic at a strategy session in 1998 converged on
``open source software'' as the preferred
alternative \parencite{Peterson_2018, OSI-web-history}. Following
extensive promotion, the term quickly gained traction in industry, in
the media, and among the public, with early support from prominent
figures in the community, including
Torvalds \parencite{OSI-web-history}. By contrast, Stallman rejected
the term on the grounds that it carries its own
ambiguity \parencite[77--78]{Stallman_2015_ch-14}
(Section~\ref{sec:misconceptions-open}) and, more importantly, for
shifting the focus away from ``the ideas of freedom, community, and
principle'' \parencite[70]{Stallman_1999}.

The Open Source Initiative was founded as part of this rebranding
effort (Section~\ref{sec:definitions}). The term ``Open Source'' was
registered as a certification mark (\ie a trademark for application to
other people's products)
\parencite[174]{Perens_1999}, with the trademark conditions spelled
out in the Open Source Definition \parencite{OSI-web-definition}
(Section~\ref{sec:definitions}). Use of the term ``Open Source'' is
thus linked to the Open Source Definition, and the Open Source
Initiative was established primarily to manage the
trademark \parencite[174]{Perens_1999}.

More broadly, the birth of the open source movement in the late 1990s
finally made explicit the ``multidimensional scattering''
\parencite[6]{Fogel_2017} of opinions that had emerged across the FOSS
community by that point (Section~\ref{sec:name-summary}). Importantly,
the movement introduced vocabulary and concepts necessary for
education and advocacy about FOSS on pragmatic grounds
\parencite{OSI-web-history}, distinct from the ideological framing of
the free software movement \parencite[7]{Fogel_2017}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
