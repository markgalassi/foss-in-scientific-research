\subsection{Legal framework}
\label{sec:legal}

In Section~\ref{sec:name} we defined FOSS and its antithesis,
proprietary software, in terms of the licence that accompanies a
computer program. Here we outline the legal basis for this
distinction. Perhaps counterintuitively, the existence of FOSS is
predicated on the notion of copyright, so we begin by outlining
relevant concepts (Section~\ref{sec:copyright}). We then discuss how
these concepts apply to software (Section~\ref{sec:licences}),
alongside some practicalities related to releasing code as FOSS
(Section~\ref{sec:practicalities}).

Our aim is to provide a brief overview of these issues aimed at the
research community (Section~\ref{sec:plan}). We emphasize that the
full picture is considerably more complicated than what we present
here, owing in part to the ambiguities in terminology discussed in
Section~\ref{sec:name}. Additional contributing factors are variation
in legislation by country and across jurisdictions, and old and new
debates about interpretation and application. Relevant technical
detail is beyond the scope of this paper, however, so we point readers
to \parencite{StLaurent_2004, Rosen_2005} for comprehensive accounts,
to \cites[10--14]{Brasseur_2018}[ch.~9]{Fogel_2017} for useful
summaries, and to \parencite{Morin-et-al_2012} for a practical guide
aimed specifically at scientists.

\subsubsection{Basics of copyright}
\label{sec:copyright}

The vast majority of countries worldwide adhere to a framework to
coordinate copyright legislation internationally. Within this
framework, the original expression of an idea through sound, imagery,
or text is automatically protected by copyright, such that authors
have exclusive rights to commercial exploitation of their work (\eg by
selling copies of it, or by displaying it publicly). Broadly,
copyright law places restrictions on use, modification, and
distribution of the work, and of works derived from it, for a set
period of time. Once that period lapses, the work is in the public
domain --- it is uncopyrighted. At this point, no one can claim
ownership of it, and anyone can exploit it commercially.

Generally, if the work is produced by an employee to their employer's
specification, then the holder of the copyright is the employer, not
the employee. There are exceptions, however: for example, in some
countries the work of government employees ``defaults'' to the public
domain. Furthermore, in some jurisdictions the work can be placed in
the public domain before the required time has passed. To this end,
the copyright holder must provide an explicit statement that they
voluntarily waive their rights in the work
(Section~\ref{sec:practicalities}).

Across jurisdictions the copyright holder can specify which rights, if
any, are granted to recipients of the work, by way of a licence. The
licence is a legal document that stipulates how the work and its
derivatives may be used (technically, the copyright holder is the
licensor, the recipients are the licencees). For example, if the
licence states ``All rights reserved'', then the rights to use and
repurpose the work remain with the copyright holder
\parencite[11]{Brasseur_2018}.

Licences can be grouped into two broad categories, based on what
restrictions they impose on further distribution of the work and of
its derivatives, whether non-commercially or
commercially. Specifically, copyleft licences require anyone who
distributes copies of the work, with or without changes, to do so
under the same terms as the original copy, or under equivalent ones;
non-copyleft licences do not include a requirement to this effect.

At a conceptual level, the significance of copyleft is to protect the
rights originally granted to recipients of the work by the copyright
holder. The outcome is that no one, not even the copyright holder, can
ever deprive others of those rights. By contrast, non-copyleft allows
anyone to remove some or all of those rights in redistribution. In
practice, then, recipients of a derivative of the work may face
greater restrictions than recipients of the original work. For this
reason, copyleft licences are considered reciprocal and protective
(also, ``share-alike''); non-copyleft ones are considered permissive
(also, ``not share-alike'').

Effectively, the distinction between copyleft and non-copyleft
licences captures a trade-off between the freedom of any one
individual to use and repurpose the work at any time, and everyone's
freedom to do so at all times
(Section~\ref{sec:misconceptions-free}). In a way, copyleft is a
``hack'' on copyright, in that it secures the opposite of what the law
seeks to achieve: a copyleft licence ensures that copyrighted work can
be freely used, modified, and distributed, and that everyone can do so
in perpetuity. Hence the play on words: ``copyleft'' is intended as
the inverse of
``copyright'' \parencite[184--185]{Stallman_2015_ch-29}.

\subsubsection{Software licences}
\label{sec:licences}

We can now discuss how the general provisions outlined in
Section~\ref{sec:copyright} apply to computer programs, with reference
to the simplified classification of software categories in
Figure~\ref{fig:licences}. We point readers
to \parencite[68]{Stallman_2015_ch-13} for an extended representation;
that level of detail is beyond the scope of this paper.

\input{./licencing-fig}

To the extent that a computer program embodies its author's original
creation, it is subject to the automatic protection provided by
copyright law \parencite[70]{Stallman_2015_ch-13}. With few exceptions
(\eg the work of government employees in some jurisdictions;
Section~\ref{sec:copyright}), by default any program is thus
\emph{copyrighted software}. As discussed in
Section~\ref{sec:copyright}, a licence can be used to stipulate what
restrictions apply to the program and to its derivatives. Through the
licence, the author specifies which rights they retain and which they
grant to others, in terms of use, modification, and distribution.

Building on the discussion in Section~\ref{sec:definitions}, a
\emph{FOSS licence} can be defined generically as one that grants
users rights to run the software, to study it, to modify it, and to
redistribute it in original or modified form. A \emph{proprietary
  licence} is one that does not meet this definition, implying that
some of those rights remain with the copyright holder. For example,
the licence may state that recipients are allowed to use the program,
but they are not allowed to modify it, nor to distribute copies of it.

A FOSS licence is termed \emph{copyleft} if it requires that
distribution of copies of the program, with or without changes, occur
under the same terms as the original copy (\ie with the same licence,
or an equivalent one). Conversely, a FOSS licence is termed
\emph{non-copyleft} (or, more commonly, \emph{permissive}) if it does
not include such a requirement. In practice, the difference is as
follows. In one case, modified versions of the program cannot be
distributed as proprietary software; in the other, they can. In other
words, copyleft does not allow derivative programs that are
proprietary, whereas non-copyleft does.

Widely used copyleft licences are the GNU General Public License
(GPL) \parencite{GPL-web}, the GNU Lesser General Public License
(LGPL) \parencite{LGPL-web}, and the Mozilla Public License
(MPL) \parencite{MPL-web}. Widely used non-copyleft ones are the
Apache License \parencite{Apache-web}, the 2-clause BSD licence (also
called ``simplified BSD'' or ``FreeBSD'') \parencite{OSI-web-BSD,
  GNU-web-FreeBSD}, and the MIT licence (more precisely, the Expat
licence) \parencite{OSI-web-MIT, GNU-web-Expat}. Within each category,
some licences are more restrictive (and thus more reciprocal and
protective), others less so (and thus more permissive)
(Section~\ref{sec:copyright}). Overall, the GPL and the MIT licence
sit at either end of this spectrum \parencite[14]{Brasseur_2018}.

Finally, a program can be \emph{uncopyrighted software} (or, more
commonly, \emph{public-domain software}); we outlined some mechanisms
to this effect in Section~\ref{sec:copyright}. Any program for which
the source code is in the public domain is FOSS, in that it can be
freely used, studied, modified, and distributed. There are no
restrictions on distribution, therefore derivative programs can be
proprietary \parencite[70]{Stallman_2015_ch-13}. A point to note here
is that since public-domain software is not covered by copyright, by
definition it cannot be subject to a licence
(Section~\ref{sec:copyright}). As we discuss in
Section~\ref{sec:practicalities}, this is an issue to consider when
releasing code as FOSS.

\subsubsection{Practicalities}
\label{sec:practicalities}

In practice, all that is required to specify the licencing terms for a
computer program is to include a file reporting the full text of the
licence; typically, this file has the name ``LICENSE'' or ``COPYING''.
Additionally, a short note is added as a comment at the top of each
source code file, stating the copyright date, the name of the
copyright holder, the name of the licence, and the location of the
full text of the licence \parencite[20--21]{Fogel_2017}. If a piece of
software does not include a licence, it cannot be legally used,
modified, or distributed (Section~\ref{sec:copyright}): doing so would
be an infringement of copyright, potentially resulting in legal
action \parencite[30]{Brasseur_2018}.

The simple procedure outlined above is complicated by the large number
of licences available. Anyone seeking to release a program as FOSS can
consult information compiled by licence-certifying organizations in
the FOSS community, such as the list of licences produced by the Free
Software Foundation \parencite{GNU-web-list}, the Open Source
Initiative \parencite{OSI-web-list}, the Debian
Project \parencite{Debian-web-list}, the Fedora
Project \parencite{Callaway_2020}, and the Linux
Foundation \parencite{SPDX-web-list}. These organizations review
existing licences to determine which meet the requirements for free
and/or open source software, and to examine issues relating to licence
compatibility. Some organizations are also directly involved in
managing specific licences. For example, the Free Software Foundation
holds the copyright to the GPL (Section~\ref{sec:licences}), and it
coordinates work relating to this licence.

The lists of FOSS licences approved by the various organizations do
not overlap fully. In some cases, the discrepancy arises for practical
reasons, for instance if a licence is yet to be reviewed by a given
organization \parencite[20]{Fogel_2017}. In other cases, the
discrepancy reflects variation in philosophical and political
views. For example, licences approved by the Free Software Foundation
are compliant with the Free Software Definition; those approved by the
Open Source Initiative are compliant with the Open Source Definition
(Section~\ref{sec:definitions}). Generally, most of the licences
approved by the Free Software Foundation are also approved by the Open
Source Initiative, and many of those approved by the Open Source
Initiative are also approved by the Free Software
Foundation \parencite[69]{Stallman_2015_ch-13}. In this context, the
terms ``free'' \vs ``non-free'', and ``open source'' \vs ``closed
source'', can be used to indicate whether a licence is approved by one
or the other organization (Table~\ref{tab:definitions}). We emphasize,
however, that the overlap between the two lists far outweighs the
discrepancies --- with few exceptions, then, the terms ``free'' and
``open source'' refer to the same body of computer programs
(Section~\ref{sec:name-summary}). In most cases, software that is free
is also open source; in slightly fewer cases, software that is open
source is also free \parencite[8]{Brasseur_2018}.

One notable discrepancy between the two lists relates to public-domain
software (Section~\ref{sec:licences}). Both organizations view such
software as free and/or open source, but they differ in their approach
to it \parencite{GNU-web-public-domain, OSI-web-public-domain}. In
particular, the Creative Commons CC0 public domain
dedication \parencite{CC-web-CC0} is a widely used legal instrument
for releasing material into the public domain. Technically, CC0 is a
waiver text, not a licence (it includes a fallback licence option for
application in jurisdictions that have no concept of dedicating work
to the public domain, by way of the copyright holder waiving their
rights in the work; Section~\ref{sec:copyright}). Creative Commons CC0
is approved as a free software licence by the Free Software
Foundation \parencite{GNU-web-CC0}. However, it is not approved as an
open source licence by the Open Source
Initiative \parencite{OSI-web-CC0}, on the grounds that public-domain
software cannot be subject to a licence (Section~\ref{sec:licences}).

This example illustrates only one of the many complications that
surround software licencing, and we refer readers to the resources
listed at the beginning of the section for additional
information. Sections~\ref{sec:gsl} and \ref{sec:closing} include
discussion of the issue in relation to research and scholarship
specifically.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
