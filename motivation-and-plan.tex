\section{Motivation and plan}

\begin{displayquote}
  \emph{An anthropologist and a physicist walk into a bar\dots Neither
    of them drinks, but they sit down for ten years and discuss the
    role of software in science.  They delve into reproducibility,
    software freedom, quality of research, case studies, emacs, and
    many other topics.  This paper presents some of their analyses and
    conclusions.}
\end{displayquote}

In 2020 the barriers to reproducible research publications have almost
entirely disappeared.  Thanks to \emph{network ubiquity}, \emph{data
  format standardization}, \emph{scientists' training in software
  development}, and \emph{widespread availability of high level tools
  and build systems}, a reasonable baseline level of reproducibility
is an attainable goal.  In 2020 non-reproducible research is a sign of
sloppy research or communication.

A confluent event is the increased awareness of the perils of
non-reproducible research.  In the last decade there has been much
discussion of a ``reproducibility crisis''\footnote{The word
  \emph{crisis} evokes a specific period of trouble, so it might not
  be the best to describe a problem that has existed since the birth
  of scientific publication.  Fortunato has characterised it as a
  chronic condition of science rather than a crisis
  \cite{fortunato2020chronic}.}.  Publications have examined this
crisis from the standpoint of researchers, mentors, journals, editors,
reviewers, institutions, integrity, danger
\autocite{ioannidis2005most,freedman2010lies}, characterization of
reproducibility \autocite{munafo2020research}, and best practices for
reproducible research
\autocite{wilson2014best,Jimenez-et-al_2017,Barba2012,national2019reproducibility}.

Some articles have cartoon guides.  Some have rants
\autocite{mccarty2008physicsSoftwareRant,callaway2009pointsOfFail}.
Some define acronyms specific to the characterization of reproducible
research -- the arrival of acronyms is a sad milestone, but also an
indicator that the subject has come of age.

Given the primacy of custom software in research, our focus will be on
understanding the relationship between scientific reproducibility and
software freedom.

The ``best practices'' articles referenced above offer helpful guides
to improve the situation, but for a long time some key scientific
software projects have emphasized more rigorous standards of
reproducibility.  Most of these fall into two major categories:
\begin{inparaenum}[(a)]
\item software delivered to institutions with a tradition of strict
  engineering requirements, such as the US department of defense; and
\item software developed by followers of the free software movement.
\end{inparaenum}

The former type falls mostly in the engineering category.  Software
with this scope has a long and strong tradition of ``verification,
validation, and accreditation'' (VV\&A), but these engineering
practices have had limited overlap with academic software.

On the other hand the free software movement grew up in the
environments of basic scientific research.  The links between
free/open-source software and software reproducibility are profound and
multifaceted, dating back to the birth of the GNU project in 1983.

We will explore these links in this article.

Just as early societal organizations were influenced by the terrain in
which they grew, so were early software distribution approaches
closely linked to the relationship to hardware cost and availability,
and to the nature of media and (later) networks.  These conditions
form a sort of ``terrain'' or ``landscape'' for the development of
software distribution and licensing approaches.

Recognizing the importance of this terrain, after defining terms, we
will take a historical jaunt along the paths leading to modern
research software and modern licensing practices.  We will discuss the
birth of the free software movement around 1983, and show how it
spawned the development of key pieces of software needed for
scientific research.

Then we will present some case studies: some early software in brief,
and then the GNU Scientific Library in greater depth.  Based on these
case studies we will offer our own prolegomena for research software.



% FIXME moved from preliminaries, to review content and links %%%%%%%%%%%%%
% 
% There is also ambiguity, and some confusion, in terminology linked to
% reproducibility and related concepts. We rely on the definitions given
% in the recent report on
% \emph{Reproducibility and replicability in science}
% %% \citetitle{NASEM_2019}
% by the U.S. National Academies of Sciences, Engineering and Medicine
% (NASEM \cite{NASEM_2019}).
% %% \citeauthor{NASEM_2019}.
% Here, reproducibility is defined as
% ``obtaining consistent computational results using the same input
% data, computational steps, methods, code, and conditions of
% analysis'' \parencite[p.~1]{NASEM_2019}. Reproducibility is distinct from
% replicability, which is defined as ``obtaining consistent results
% across studies aimed at answering the same scientific question, each
% of which has obtained its own data'' \parencite[p.~1]{NASEM_2019}. In
% turn, both reproducibility and replicability are distinct from
% generalizability, namely ``the extent that results of a study apply in
% other contexts or populations that differ from the original
% one'' \parencite[p.~1--2]{NASEM_2019}.
% % 
% % TODO we may review to fine-tune level of detail here, in light of
% % what is required specifically in the context of the paper, possibly
% % incorporating some of the below:
% % 
% % So, we limit ourselves to discussing a few key concepts that we find
% % useful in thinking through the issues, reflecting our use of related
% % terminology throughout this paper.
% % 
% % % - Peng 2011 spectrum, low-hanging fruit?
% % 
% % % - Goodman et al 2016 three levels of reproducibility
% % 
% % % - Stark 2018 pre-producibility?

% The NASEM report \textcite{NASEM_2019} focuses on computational
% reproducibility and related issues as they apply specifically to
% scientific research. We broaden the scope to research more generally,
% which we define as the process of knowledge generation and
% dissemination. % TODO check standard definitions
% Nowadays, anyone inolved in this process is effectively a
% \emph{computational researcher}, to the extent that they use software
% to handle, process, and store information. Against this designation,
% the emphasis shifts from viewing software as a tool, to viewing it as
% an integral part of the research process. By broadening the scope, we
% aim to highlight the relevance of the discussion to disciplines in the
% humanities and in the social sciences, and in some areas of the
% natural and medical sciences, in which the ``software-as-tool''
% perspective has tended to prevail. % TODO review --- is this a
% % convulted way to convey STEM vs. other research areas?

% We also recommend extending these practices beyond the research
% process itself to the surrounding infrastructure (research support,
% admin, publishing).

% % TODO possibly discuss context of research production --- whether in
% % academia, research laboratories, industry, etc. So we depart from
% % the academic context specifically, as specific incentives apply here
% % which apply less markedly, or not at all, in other contexts
% % (e.g. publish or perish).  % TODO see refs about
% % % "knowldege work", and academia as "extreme knowledge work"
% % %
% % These peculiarities//features//idiosyncrasies of academia shape the
% % debate//discourse around reproducibility and related issues in ways
% % that are less relevant, or not at all, in other areas of the research
% % community//to other groups enaganged with knowledge
% % production. % TODO unpack/expand?
 
% % TODO possibly add plot of frequency of terms to illustrate recent
% % transition from "open science" to "open research/scholarship"?
% % defined as "open research" == process, "open science" the subset of
% % open research focusing on STEM, "open scholarship" as the broader
% % ecosystem (e.g. including open access)

% % TODO note that the focus on research (vs. science) is also relevant
% % (and cute!) in that etymologically it means ``search
% % again'', % TODO check
% % emphasizing the benefit of automating repetive tasks, which is one
% % of many benefits of the approach//pipeline//workflow we champion
% % here.

% % TODO add that focus on reproducibility is relevant here given reach
% % across fields, whereas there are limits as to what replicability
% % applies to, e.g. Nosek and Errington 2020 PLoS

% % TODO is a point to make that the focus on freedom is, in fact, more
% % aligned with aims of the open science/research movement than open
% % source itself?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "foss-in-scientific-research"
%%% End:
