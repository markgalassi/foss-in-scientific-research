\section{Closing}
\label{sec:closing}

We conclude with two remarks, to illustrate the relevance of the
preceding discussion to research and scholarship today
(Section~\ref{sec:scene}). The first remark is aimed specifically at
researchers. To this end, we return to the common interpretation of
``open source'' as ``open code'' outlined in Section~\ref{sec:plan},
and in particular to the scenario relating to software hosted in an
online repository that is publicly accessible. It should be clear from
the material in Sections~\ref{sec:name} and \ref{sec:legal} why, as
noted in Section~\ref{sec:plan}, public access to the source code is
neither necessary nor sufficient for a computer program to qualify as
FOSS.

We can make this scenario more concrete with reference to software
hosted on \url{https://github.com}, a platform popular among
researchers. The website is run by GitHub, Inc., a subsidiary of
Microsoft Corporation. The GitHub platform provides online services
for software development, and specifically distributed version control
and source code management using Git \parencite{git-web}. Git is FOSS
under the GPL (Section~\ref{sec:licences}); it was created by Torvalds
and others for development of the Linux kernel
(Section~\ref{sec:history-1990s-linux}). Additional features on the
platform, including its ``social networking'' functions and other
tools for collaborative development, are based on a proprietary web
infrastructure --- that is, the web interface itself is not FOSS.

In the context of open science, there is growing pressure on
researchers to share programs they develop (Section~\ref{sec:plan}). A
common recommendation is to host the code on the GitHub
platform \parencite[\eg][]{Blischak-et-al_2016,
  PerezRiverol-et-al_2016, Wilson-et-al_2017}. As noted in
Section~\ref{sec:plan}, simply hosting source code in a publicly
accessible repository on \url{https://github.com} does not amount to
``open sourcing'' a piece of
software \parencite[\eg][]{Jimenez-et-al_2017}: the software is free
and open source only if it is released under an appropriate licence
(alternatively, the source code must be in the public domain;
Section~\ref{sec:licences}).

And yet, in our experience, this approach to ``open sourcing''
software is common among researchers. For example, a convenient way to
share code with others is to upload it to a public repository on
GitHub. Crucially, all too often there is no licence
(Section~\ref{sec:practicalities}). Without a licence, nobody has
permission to use, modify, or distribute the code in the
repository \parencite{CAL-web-no-license}. If multiple collaborators
contributed to development of the code, then ``nobody'' includes each
one of them. Under the GitHub terms of service, other registered users
on the platform can (i) view code hosted in a publicly accessible
repository, and (ii) make a copy of the repository into their own
account (via GitHub's ``fork'' function). Technically, however, they
are not allowed to use, modify, or distribute the code --- doing so is
a violation of copyright.

Clearly, researchers who share code in this way do not intend to
disallow its use and repurpose. Rather, they typically aim to
facilitate collaboration, enabling others to reproduce their work and
to build on it --- precisely the reasons the code should be released
as FOSS in the first place! To be clear, we are not dwelling on this
example as a criticism to well-intentioned researchers. Instead, we
wish to highlight that it is important for anyone who contributes to
the research process to acquire a working knowledge of FOSS, so that
they can make informed decisions about software as an integral part of
their day-to-day workflow (Section~\ref{sec:scene}). We hope that the
primer in Section~\ref{sec:primer} proves useful in this
regard. Increasing awareness seems key, given that between 2008 and
2015 only around 20 to 30\% of all repositories hosted on the GitHub
platform included a licence \parencite{Balter_2015}. These figures are
all the more striking considering that, since 2013, the web interface
has actively encouraged users to specify a licence when creating a new
repository \parencite{Haack_2013}: the initialization menu includes a
``Choose a license'' checkbox, which brings up a ``license picker''
with a list of commonly used options (the drop-down menu defaults to
``License: None'').

The lessons learnt from the case study in Section~\ref{sec:gsl}
underscore that lack of clarity in licencing leads to lost opportunity
and wasted effort: the demise of the CLAMS library is a clear example
(Section~\ref{sec:lessons-public}). In the long run, the restrictions
imposed by proprietary licencing may lead to an analogous outcome, as
demonstrated by the downward trajectory of the
\citetitle{Press-et-al_1986} package
(Section~\ref{sec:lessons-proprietary}). Both lessons echo the Unix
story (Section~\ref{sec:history}). The long-term prospects of the
operating system that ``changed the entire path of computer
technology'' \parencite[ix]{Kernighan_2020} were thwarted by the lack
of a clearly defined approach to licencing: the ``liberal''
distribution of source code in the early years (\eg to educational
licencees) clashed with later attempts to restrict circulation for
commercial gain \parencite[157]{Kernighan_2020}
(Section~\ref{sec:history-origins-unix}). The legacy of Unix is
substantial; sadly, the system itself succumbed to the confusion and
``legal wrangling'' \parencite[157]{Kernighan_2020} that ensued from
the clash (Sections~\ref{sec:history-1980s-unix} and
\ref{sec:history-1990s-unix}).

A related lesson to emerge from the case study is that a careful
approach to licencing is key to the resilience of software projects
and, ultimately, to research quality and sustainability. In
particular, development of the FOSS resources discussed in
Section~\ref{sec:gsl} involved technical and organizational challenges
linked to creating, maintaining, and distributing large and complex
codebases, and to managing projects that rely heavily on feedback
between developers and researchers. Arguably, the success of any such
effort is best assessed in terms of longevity and widespread uptake,
rather than by popularity of a piece of software over a short period
of time. Decades on, the SLATEC library, Netlib, and GSL are still
robust, relevant, and widely used (Sections~\ref{sec:lessons-public}
and \ref{sec:lessons-free}) --- evidence that upfront investment in
FOSS can lead to substantial long-term dividends in terms of research
quality and sustainability (Section~\ref{sec:scene}).

Building on this lesson, the second concluding remark is aimed more
broadly at researchers, support staff, administrators, publishers,
funders, and anyone else in the research community with an interest in
open scholarship (Section~\ref{sec:scene}). The implication here is
that the community as a whole stands to benefit from a ``FOSS-first''
approach to both the research process itself and the surrounding
infrastructure. For example, we can ask whether well-intentioned
researchers based at a university should indeed rely on services
provided by a company like GitHub, Inc.\ in their day-to-day work. In
our experience, many do so out of convenience, because equivalent FOSS
infrastructure is not available through the university.

As noted in Section~\ref{sec:scene}, related discussions are often
framed in terms of the convenience of proprietary products \vs the
cost-effectiveness of FOSS solutions. The principle of software
freedom is typically ignored, and with it the issue of control over
the digital tools and services that enable the research process. To
the extent that FOSS is itself a contribution to human
knowledge \parencite[234]{Stallman_2015_ch-39}, reframing the
discourse in terms of principle, based on an ethical perspective, can
bolster other ``open'' initiatives in research and scholarship, in
support of the overarching aim: knowledge production and
dissemination.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
