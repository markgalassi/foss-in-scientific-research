% LICENSE: This work is licensed under a Creative Commons
% Attribution-Sharealike 4.0 International License.
% https://creativecommons.org/licenses/by-sa/4.0/

% you can typeset this article with "latexmk -pdf main.tex"

\documentclass[a4paper]{article}

\usepackage{geometry}
\usepackage{pdflscape}
\usepackage{orcidlink}
\usepackage{hyperxmp}

\usepackage[nottoc]{tocbibind}

\usepackage[%
citestyle=numeric-comp,%
giveninits=true,%
sortcites=true,%
sorting=none,%
maxbibnames=10,%
mincrossrefs=99,%
terseinits=true,%
url=false,%
doi=true,%
eprint=false,%
isbn=false,%
alldates=edtf,%
seconds=true,%
defernumbers=true
]{biblatex}

\addbibresource{references.bib}

\DeclareNameAlias{default}{last-first}%
\DefineBibliographyExtras{english}{%
  \renewcommand*{\finalnamedelim}{\addcomma\addspace}%
}%
\renewcommand*{\revsdnamepunct}{}

\renewcommand*{\multicitedelim}{\addsemicolon\space}

\usepackage{authblk}

\usepackage{paralist}
\usepackage{csquotes}

% formatting macros
\usepackage{xspace}

\newcommand{\ie}{i.e.\@\xspace}
\newcommand{\eg}{e.g.\@\xspace}
\newcommand{\etc}{etc.\@\xspace}
\newcommand{\vs}{vs.\@\xspace}

\usepackage{graphicx}
\usepackage{tikz}
% NOTE modified from
% http://texample.net/tikz/examples/decision-tree/
\tikzset{
  treenode/.style = {shape=rectangle, rounded corners,
                     draw, align=center,
                     top color=white, bottom color=gray!25,
                     font=\normalsize\ttfamily},
  root/.style     = {treenode},
  env/.style      = {treenode}
}

\usepackage{xcolor}
\usepackage[framemethod=tikz]{mdframed}

\usepackage{titlesec}
\titleformat*{\subparagraph}{\itshape}

% tables
\usepackage{booktabs}
\usepackage{rotating}
\usepackage[flushleft]{threeparttable}

\hypersetup{%
  pdfauthor={Laura Fortunato, Mark Galassi},%
  pdflang={en-gb},%
  pdfkeywords={free and open source software (FOSS), GNU Scientific
    Library (GSL), open research, open scholarship, open science,
    reproducibility},%
  bookmarksnumbered=true,%
  hidelinks%
}



% licencing
\usepackage[type={CC},%
modifier={by-sa},%
version={4.0}%
]{doclicense}



\title{The case for free and open source software\\
  in research and scholarship}

\author{Laura Fortunato$^{1,2}$ \orcidlink{0000-0001-8546-9497} and
  Mark Galassi$^3$ \orcidlink{0000-0002-3279-2693}}

\affil{%
  $^1$ Institute of Cognitive and Evolutionary Anthropology\\
  University of Oxford\\\bigskip%
  $^2$ Santa Fe Institute\\\bigskip%
  $^3$ Space Science and Applications Group\\
  Los Alamos National Laboratory%
}

\date{}

\begin{document}
\maketitle

\vfill%
\noindent \fullcite{Fortunato-Galassi_2020_repository}

This is the ``author accepted manuscript'' version, or post-print, of
\fullcite{Fortunato-Galassi_2021}.

The article is part of the theme issue ``Reliability and
reproducibility in computational science: implementing verification,
validation and uncertainty quantification \emph{in
  silico}''. \textsc{doi:}
\href{http://dx.doi.org/10.1098/rsta/379/2197}{\texttt{10.1098/rsta/379/2197}}

\doclicenseThis

\clearpage
\pdfbookmark[1]{Contents}{contents}
\tableofcontents

\clearpage
\input{abstract}

\clearpage
\input{introduction}

\input{primer}

\input{name}

\input{legal}

\input{history}

\input{gsl}

\input{closing}

\phantomsection%
\addcontentsline{toc}{section}{Acknowledgements}
\section*{Acknowledgements}

We thank Jeremy Allison, Jim Blandy, and Tom Tromey for discussion,
and Malika Ihle, Adam Kenny, and Rowan Wilson for feedback on the
manuscript. We are also grateful to those who took part in discussions
hosted in September 2020 by \href{https://ox.ukrn.org/}{Reproducible
  Research Oxford} and by
\href{https://anthrologue.org/}{\texttt{anthrologue}}, and to the
reviewers for providing positive, constructive comments.

\phantomsection%
\printbibliography[heading=bibintoc]

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
