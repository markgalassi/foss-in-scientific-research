We thank the \rrs for the positive feedback and the helpful comments
on the manuscript. A point-by-point response to the comments is
below. The manuscript is improved following the suggested revisions,
and we are grateful for the input!

\section*{Response to \rr 1}

\begin{itemize}

\item \emph{In Section 2 we read “In other words, we seek to extend
    the discourse surrounding FOSS from the usual context of open
    science to the broader one of open research.” It is not exactly
    clear to me how open science differs from open research. Perhaps
    the text could elaborate on this.}

  We have revised the text in the corresponding paragraph and in the
  following one to unpack, and hopefully clarify, the distinction
  between open science and open research. In brief, we view ``open
  research'' as broader than ``open science'', in that it includes all
  disciplines, not just the sciences.

  The distinction seems necessary because in some scholarly traditions
  the humanities in particular are viewed as separate from the
  sciences. As a result, many researchers in the humanities tend to
  feel that discussions about open science do not apply to them ---
  or, worse, that they are actively excluded from these discussions.
  
\item \emph{Section 3\\
    “We seek to complement this literature with a primer suitable for
    novices (Section 1)”\\
    Should be 3.1}

  The reference is actually to Section~\ref{sec:plan}, where we first
  mention that Section~\ref{sec:primer} is pitched at novices. We have
  revised the corresponding paragraph to resolve the ambiguity noted
  by the \rr.

\item \emph{Section 3.1:\\
    “The free software movement is “a campaign for freedom for users
    of software” [21] — that is, freedom from restrictions on use,
    study, modification, and distribution of software. The open source
    movement is, effectively, “a marketing program for free software”
    [12, p. 7], centred on the practical advantages that can derive
    from that freedom. Taken together, the two movements represent the
    ends of a continuum…”\\
    I was a bit puzzled by the phrase “ends of a continuum”, the two
    organizations seems to have a large overlap, and do not lie on
    opposite ends of a continuum.}

  We have removed the reference to the ends of continuum and revised
  the corresponding paragraph.
  
\item \emph{Section 3.1\\
    “The Free Software Definition and the Open Source Definition are
    equivalent for this purpose”\\
    What purpose? I suppose the authors refer to the license, but this
    was not entirely clear.}

  Indeed! In any case, we have revised this section entirely in
  response to point~\ref{itm:rr2-point-1} raised by \rr 2, and in
  doing so we have taken the opportunity to also clarify the
  corresponding text.

\end{itemize}

\section*{Response to \rr 2}

\begin{enumerate}

\item \label{itm:rr2-point-1} \emph{In my opinion, section 3.1 is
    relatively weak compared to other sections. The writing is rather
    roundabout, and as a reader I end up with a reasonably clear
    definition of what FOSS is about, but a lot of confusion around
    the definition of open source. It would be good to make comparison
    between the two more succinct and systematic, and to ensure that
    the definition for open source you provide there is as clearly
    presented as the one about FOSS. A comparison table or figure
    between foss and open-source might actually provide help to the
    reader here as well.}

  The difficulty here, reflected in the confusion picked up by the
  \rr, is that there is no succinct definition of open source. In
  large part, this is because the term ``Open Source'' is registered
  as a trademark by the Open Source Initiative, and the Open Source
  Definition is a verbose document which effectively spells out the
  trademark conditions (Section~\ref{sec:history}). The text of the
  Open Source Definition is available in full at
  \url{https://opensource.org/osd}.

  As a way around this issue, we have chosen to provide a generic
  definition of FOSS in Section~\ref{sec:name} based on the Free
  Software Definition, which is sufficiently broad to apply both to
  free software and to open source software. We then build on this
  generic definition by providing technical information on licences in
  Section~\ref{sec:legal}, supplemented by historical notes in
  Section~\ref{sec:history} (including information about the legal
  status the Open Source Definition, and its link to the trademark
  conditions).

  In response to the \rr's feedback, to address the confusion we have
  substantially restructured Section~\ref{sec:name}, which now
  includes three subsections. We have also substantially revised the
  text to include a clarification of the relationship between the Free
  Software Definition and the Open Source Definition, spelling out the
  practical reasons we rely on the former for a generic definition of
  FOSS (\ie that the latter is not as easily paraphrased, nor readily
  summarised; Section~\ref{sec:definitions}).

  Additional revisions to the text include, in
  Section~\ref{sec:misconceptions}, an expanded discussion of the
  distinction between non-commercial \vs commercial software; a more
  streamlined discussion of source-available software and its
  relationship to open source; and a revised discussion of
  availability of source code to users \vs the public at
  large. Finally, we have expanded the summary text in a dedicated
  subsection (Section~\ref{sec:name-summary}), including an explicit
  justification of the approach to terminology used in the paper,
  which is supplemented by a table of key terms
  (Table~\ref{tab:definitions}).

  We had in fact considered adding a figure in the original
  submission, but eventually we gave up on the idea, as this would
  require introducing upfront much of the material discussed in later
  sections, and including information about licences that is beyond
  the scope of the paper --- an example figure is available at
  \url{https://www.gnu.org/philosophy/category.svg}. The summary table
  is a much better option here, and we thank the \rr for this
  suggestion.

\item \emph{The introduction, Sections 1 \& 2, and especially 3.2 are
    very clear to me, succinct and well-written, so I have no major
    comments there.}

  Thank you! In any case, we have taken the opportunity to make
  several improvements throughout these sections. In particular, we
  have revised Section~\ref{sec:legal} in both structure and content,
  largely in response to point~\ref{itm:rr2-point-4a} below.

\item \emph{The Case Study is well-written, but suffers to a lesser
    extent from the roundabout writing style in 3.1. I believe it may
    be good to make it slightly more concise (e.g.\
    \textasciitilde10--20\% shorter).}

  We have revised the section to remove some repetitive and redundant
  text, although we have not achieved the target set by the \rr. Apart
  from these tweaks, we have struggled to identify content to
  trim. This is largely because we wanted to draw a self-standing
  picture of the state of numerical analysis in the 1990s. To a lesser
  extent, we also feel that readers may benefit from learning about
  some of the ``internal dynamics'' involved in establishment and
  development of a successful free software project.

\item \emph{In the conclusion, there are a few things I'd like to
    raise:}

  \begin{enumerate}
      
  \item \label{itm:rr2-point-4a} \emph{the point made about the
      copyright implications of unlicensed public repositories in
      GitHub is a valid one, and should be generalised and expanded a
      bit more. First of all, please clarify whether these copyright
      issues are universal, or apply only to the copyright laws in
      some countries and not others.}

    Yes, our understanding is that the issues we raise in relation to
    GitHub repositories in Section~\ref{sec:closing} do indeed apply
    internationally, as most countries worldwide adhere to a joint
    framework to coordinate copyright legislation. We have revised
    Section~\ref{sec:copyright}, where we introduce the basics of
    copyright, to include a discussion of this point in general terms,
    and we link to it in Section~\ref{sec:closing}.

  \item \emph{GitHub does have a nudging system that discourages
      users from creating repositories without licenses. I would
      suggest that the authors create an empty GitHub repository to
      observe that nudging system, and then perhaps make a few remarks
      about how this mechanism is insufficient. This may seem trivial
      and detailed, but I think the overarching point is important
      enough to be made explicitly, and probably along the lines of
      this: adding a few algorithmic nudges is insufficient when it
      comes to avoiding legal issues.}

    We have included a paragraph making this point, to highlight that
    increased awareness of the issues seems key.

  \item \emph{The point about the importance of FOSS from a tool-level
      perspective needs a bit more elaboration in the
      conclusions. Please explicitly refer back to the CLAMS example
      there, and perhaps also relate it to other literature that
      discusses e.g.\ issues with proprietary binary file formats?}

    We have tweaked the text in response to these suggestions, but we
    have not implemented them in full, as doing so would require
    lengthening an already long paper. The issue of binary file
    formats is clearly relevant --- the proprietary nature of many of
    them is just one of the problems that come up.  A previous
    iteration of the manuscript did in fact include a discussion of
    the HDF5 project, but this example did not make it into the
    submission due to space limitation. We are not aware of references
    that address the topic from the same angle and at an appropriate
    level of analysis, suitable for the intended readers of the paper.
    
  \end{enumerate}

\end{enumerate}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main-response"
%%% End:
