% LICENSE:
% Creative Commons Attribution-Sharealike 4.0 license (a.k.a. CC BY-SA) (#ccbysa)

\documentclass[12pt,a4paper]{report}
\raggedright

\usepackage{hyperref}
\usepackage[nottoc]{tocbibind}

\usepackage[citestyle=authoryear-comp,style=authoryear]{biblatex}
\addbibresource{foss-scientific.bib}

\usepackage{authblk}
\usepackage[procnames]{listings}
\makeatletter
\def\addToLiterate#1{\edef\lst@literate{\unexpanded\expandafter{\lst@literate}\unexpanded{#1}}}
\lst@Key{add to literate}{}{\addToLiterate{#1}}
\makeatother


\title{Free and Open Source Software in Scientific Research}

\author{Laura Fortunato }
\affil{Department of Anthropology, University of Oxford \\
  \texttt{fortunato@anthro.oxford.ac.uk}}
\author{Mark Galassi}
\affil{Space Science and Applications group, Los Alamos National
  Laboratory
  \\ \texttt{mark@galassi.org}}

\date{\today}

\begin{document}
\maketitle
\tableofcontents

\abstract{

Free and Open Source Software (FOSS) has had a major impact in the broad
computer industry since the late 1990s, fueled in part by the
ubiquitousness of the GNU toolchain and the Linux kernel, but in the
world of scientific research the impact is much greater and goes back
much longer.  Starting from theoretical physics and mathematics,
spreading to bioinformatics and eventually to the burgeoning use of
rigorous and advanced computing technique in quantitative social
science, FOSS is used in the majority of research.  But as often happens
in revolutions, most researchers do not see how their tools fit in the
larger context of the software world, and research projects often suffer
from problems in this key part of their supply chain.

In this article we give a big picture view of the use of free and open
source software in scientific research, present some case studies, and
show some important insights which come from these studies and which
offer prolegomena for future research projects.

}

\section*{Notes on terminology}

\begin{description}
\item[free versus open source] ``Free software'' is software which is
  distributed with source code and is freely modifiable and
  redistributable.  For a more complete description please see the
  Free Software Foundation's materials on this subject (ref FSF
  documents).  The ``free software movement'' is a decentralized
  collective of programmers who volunteer to write free software.


  Christine Peterson coined the term ``open source'' in 1998
  \autocite{OpenSourceOrgHistory} to communicate the possible
  commercial advantages of free software to marketing-oriented
  companies, specifically in occasion of the sale of Netscape to
  America Online (FIXME: check it, and check spelling of Online in
  AOL).  In the early days there was hope that of the use of the term
  ``open source'' would work around the awkwardness deriving from the
  dual meaning of the English language word ``free'' (which can refer
  to freedom or to gratis/zero-cost).\footnote{People in the free
    software movement sometimes use the expression ``free speech
    versus free beer'' to clarify the distinction between ``free as in
    freedom'' and ``free as in gratis''.}  But the term open source
  did not gain traction in the free software movement because it
  removed the emphasis on freedom.

  Since the licenses that are considered open source have to satisfy
  the same requirements as free software licenses, free software and
  ``open source'' software refer to the same body of software.  But
  ``free'' is used to emphasize the principle of software freedom,
  while ``open source'' is used to emphasize the competitive
  advantages which can derive from that freedom.

  Acronyms such as FOSS (free and open Source Software) and FLOSS
  (Free-Libre / Open Source Software) have been coined to refer to the
  entire body of free software in such a way that everyone will
  understand them, independent of the direction from which they
  approach the topic.

  In this paper we will use the terms ``free software'' or FOSS to
  refer to this body of software, and we will make explicit reference
  to ``free'' or ``open source'' when it useful to emphasize the goals
  of a movement.

\item[kernel versus operating system] Another ambiguity arises with
  the term ``operating system'': it is used to refer to two different
  things.  For some the operating system is only the ``kernel'': that
  software layer which talks directly to a computer's hardware and
  offers and abstraction for application programs to use the hardware.
  For others the operating system is the kernel plus the collection of
  libraries and programs which makes for a fully usable computer.  The
  Debian GNU/Linux operating system, for example, provides fifty one
  thousand software packages of free software.  We will use the latter
  meaning for ``operating system'': the kernel plus the collection of
  programs shipped by the system provider.

\item[free versus proprietary and gratis versus commercial] Another
  important point of terminology is the distinction between free,
  proprietary and commercial.  Free software is what we described
  above: freely redistributable software.  \emph{Commercial} software
  is software that is distributed for profit, but it is often free (as
  in freedom).  Indeed, great fortunes have been made by
  commercializing free software.  So \emph{commercial software is not
    the opposite of free software.}  \emph{Proprietary} software is
  the true opposite of free software.  It is important to juxtapose
  ``free versus proprietary'' software, and not ``free versus
  commercial'' software.


\item[permissive versus copyleft] Within the world of free and open
  source licenses some can be called \textbf{permissive} while others
  are called \textbf{copyleft}.  Permissive licenses are free but they
  allow derived works to be proprietary.  Copyleft licenses require
  that derived works be released with the same freedoms as the
  original.

\item[Linux versus GNU/Linux] Finally: much argument surrounds the use
  of the term Linux versus the term GNU/Linux: some use one, some the
  other, to refer to a full operating system based on the Linux
  kernel.  We will use the term Linux to refer to the kernel, and
  GNU/Linux to refer to a full operating system based on the Linux
  kernel.
\end{description}

\chapter{Free software before the GNU Manifesto}

Richard Stallman's GNU Manifesto \autocite{stallman1985gnu} was
published in 1985, and it outlined a principled and carefully thought
out approach to developing a complete free (as in freedom) operating
system: a kernel and a full collection of developer and user-level
utilities.

Before the GNU Manifesto there were were several {\it de-facto}
channels by which proto-free software was distributed.  Scientists who
started research in the last twenty years will not have experienced
the older approaches to software distribution, beyond hearing stories
of punchcards and paper tape from their elders.

There are some logistical aspects of early distribution which had a
large \emph{de-facto} effect on how software was shared before 1985:

\begin{description}
\item[early hardware platforms] The first medium that allowed
  convenient distribution and sharing of software was the reel tape.
  Reel tapes for data storage were available on the UNIVAC-1 in 1951
  and the IBM 701 in 1952.

\item[paucity of networks: early ARPAnet, bitnet, UUCP] If you were in
  a physics or computer science department in the early 1980s, and
  sometimes even well into the late 1980s, it was unlikely that you
  would find the fully connected ARPAnet (now known as the internet).
  The fact that a small fraction of the research population had access
  to networks affected the effort put in to writing software for
  widespread use.

\item[relative cost of hardware and software] Focus on hardware
  revenue was the overwhelming consideration for a very long time.
  This affected corporate motivations for investement in software
  development, and it affected decisions by companies on how to
  distribute software and what relationship to keep with volunteer
  contributors.

\end{description}

The lack of a clearly stated philosophy before 1985 meant that the
nature of software distribution was almost entirely deteremined by
these logistical factors.

The UNIVAC 1 and the IBM 701 (and later 704) were the dominant
computer platforms in the early 1950s.  These were present in many
installations all over the world.  These two platforms are the ones on
for which most widely distributed software in the 1950s was written.

\section{Nature of early software and its transmission}

The very earliest software written was programmed directly from toggle
switches on computer panels.  This approach does not give much
opportunity for sharing and is not interesting to us here because it
was not a medium that allowed distribution.

Early ``storage program'' computers (UNIVAC, IBM 701/704) allowed a
program to be stored in memory.  Initially these programs were loaded
from mechanical media, such as punch cards and paper tape.  Early
computer user groups would exchange software by passing around stacks
of punch cards, but as programs got larger the stacks became
mechanically unwieldy and reel tapes became the standard medium for
distributing software.\footnote{In 1967, for example, the SHARE user
  group started distributing reel tapes instead of punch cards
  \autocite[p13]{bemer1982computing}.}

The early decades of computing were entirely driven by the business of
\emph{numerical analysis} which is the core of scientific computation
(FIXME ref Rrefethen numerical analysis).  At that time the sharing of
numerical \emph{routines} was more important than the sharing of
complete programs.  Grace Hopper first showed how a computer proram
could be written by calling many routines: ``All I had to do was to
write down a set of call numbers, let the computer find them on tape,
bring them over and do the additions.  This was the first compiler.''
(FIXME ref Marianne McKenzie ``The Amazing Grace
Hopper'', Ceruzzi pages 70s and 80s.)

This invention of Hopper's, implemented on the UNIVAC, was called the
A-0 system.  Today we call it a \emph{linker} rather than a compiler
\autocite[p.85]{ceruzzi2003history} \autocite[p.51]{knuth1980early},
and it first solved the problem of inserting into a program the
frequently-used bits of code stored at various locations on a tape.

Hopper went on to develop the A-2 system
\autocite{hopper1953influence,hopper1955automatic} and eventually
(what we would now call) actual compilers for the UNIVAC - the
\texttt{MATH-MATIC} system which allowed algebraic formul{\ae} such as
(FIXME: ref univac math-matic manual, page 2)

\begin{lstlisting}[language={[77]fortran}]
(10) X = (15*Y+3*Z) / SIN A .
(6) X(I) = A(I) + B(J, I) .
(11) IF X > Y JUMP TO SENTENCE 8 .
\end{lstlisting}

and the FLOW-MATIC language, which was intended to ``feel'' more like
a sequence of English phrases and which led to the development of the
COBOL programming language.

For our purposes the most important thing to note about Hopper's work
on the A-2 system is that it was distributed to UNIVAC users free of
charge, and users were asked to return improvements to the UNIVAC team
(FIXME: need to find direct citation).

Rich technical histories of this early period in software can be found
in \autocite{knuth1980early, ceruzzi2003history,
  campbell1995development}.  \citeauthor{campbell1995development} in
particular describes the difficulty in using primary sources for a
careful analysis of early software history
\autocite{campbell1995development}.  Primary sources, or sources based
on personal recollection, can be found in
\autocite{bemer1982computing} and CITE Remington-Rand technical
reports).

\autocite{campbell1995development,ceruzzi2003history} FIXME: discuss
how Campbell analyzes the difficulty in using primary sources for
software history.

While Hopper was developing early compiler systems for the UNIVAC,
John Backus was developing the FORTRAN programming language and
compiler for the IBM 704 computers.  FORTRAN rapidly became the most
used programming language in the scientific community at the same time
that IBM's mainframes were taking over the majority of the hardware
market.


\section{The role of the SHARE user group ...Other materials...}

[FIXME: material to be used, but not really organized yet]

See Knuth's 1980 survey, mentioned at Ceruzzi p. 86 (end) and look at
footnote 31 at page 87.

Possibly the most significant early step in the distribution of
software came with the SHARE user's group for the IBM 701 computer.
These engineers from the Los Angeles area first gathered in 1955 to
share the software they had developed for the IBM 701 and 704
computers.  Ceruzzi writes (page 88) that ``The founding of SHARE was
probably a blessing for IBM, since SHARE helped speed acceptance of
IBM's equipment and probably helped sales of the 704.''

How close was SHARE to modern free and open source developer
communities?  FIXME: base the answer on this primary source:
\cite{armer1956share}

SHARE's success spawned the development of other groups dedicated to
other computer platofrms, like DECUS for the Digital Equipment
Corporation minicomputers.

These early sharing options depended crucially on \emph{tapes}.  The
main near-line storage of data was on reel tapes, and these could be
mailed to other users of the same type of computer platform.

The importance of retrieving and handling data from tapes led early
programmer Frances ``Betty'' Holberton, to develop the first widely
used software ``routines'' which sorted data from UNIVAC tapes.

CTSS, project Mac (CITE
https://apps.dtic.mil/dtic/tr/fulltext/u2/609296.pdf) and ITS.

The final note on the link between hardware constraints and approaches
to sharing software has to do with relative costs.  Ceruzzi at page 82
shows the graph of relative hardware versus software costs as a
fraction of the total.  Hardware starts at 80\% of the cost in 1965,
and the ratio flips by 1985.  This means that early on a company like
IBM initially had a very strong interest in freely redistributable
software since its revenue came from hardware, and sofware
availability made the hardware more valuable.

This started changing in 1969 when IBM, pressured by an antitrust
lawsuit, ``unbundled'' software and started charging for it
separately.  This had many ramifications.  It is thought to have
spawned a separate software industry, but one that worked in a
positive direction for freely redistributable software was that it
gave an incentive for the development of independent operating
systems, such as MIT's ITSS and Bell Labs's UNIX.

Discussion of IBM: \autocite{campbell2008pragmatism,grad2002personal}

\section{Proto-software freedom in early compilers and operating systems}

The distribution of Grace Hopper's A-2 compiler has been cited as an
early example of the success of an early free software distribution
model.

Jean Sammet has written extensively about the history
\autocite{sammet1981history} FIXME: must find full Sammet text.

Martin Campbell-Kelly and Daniel Garcia-Swartz
(\autocite{campbell2008pragmatism}) draw a careful and well-documented
historical picture of IBM's decision-making process surrounding the
distribution of the FORTRAN compiler source code, and later on the
distribution of the fabled System/360 operating system.  They give two
reasons for the distribution of the source code: the first is that it
benefited end users.  The second is that an estimate of how much money
IBM could have made by charging for its software was about one million
dollars per year.  This commercialization was hardly worth considering
when their hardware market was two billion dollars per year.

This observation begs the question: were System/360 and the FORTRAN
compiler (and other tools IBM distributed) free and open source
software?  The answer is a qualified ``yes'': users had all the
relevant freedoms that the free software movement require, but these
freedoms were not guaranteed by a legal framework based on copyright
or contract law.  FIXME: say more about legal frameworks either here
or in a new section, citing \autocite{bollinger1999linux} and
\autocite{driscoll2015professional}

Campbell-Kelly and Garzia-Swartz paint a picture of IBM evolving under
the pressure of technological, logistical and market forces, and reach
a conclusion of an almost \emph{necessary} evolution toward a free
software model, almost as Rousseau looks at the history of humans and
demonstrates the inevitability of the origin of a certain social
structure \autocite{rousseau1984discourse}.

Their argument might be cogent in the context of what IBM's business
had become by the early 1960s.  IBM saw the first example of a company
that first introduces computers for scientific research, but then
finds that the best long-term business model is for transaction
processing.  By 1962 IBM's top customer category was the banking
industry, followed by the life insurance industry
\autocite[p.10]{campbell2008pragmatism}.  This pattern of innovation
for the purpose of scientific computing performance, followed by a
shift in focus to the transaction processing market dominated by the
banking industry, has repeated several times since.  More recently the
focus of high performance computing has also been diverted to the
``video on demand'' sector.

This shift in focus, just as software was becoming more important,
means that the big drivers for the software development and
distribution were no longer tied to the world of scientific computing
and numerical analysis.

We have drawn a picture of IBM's importance in the scientific and
engineering communities and of the importance of the SHARE user group
in that sector.  This, together with the 1950s and early 1960s
distribution model by IBM, shows that it is reasonable to say that
early software was free software, and the freedoms were motivated by
the same considerations which Stallman laid out later in the GNU
manifesto.

A question which remains is what the impact was of the lack of
Stallman's formulation.  Later generations of IBM system software
users were deprived of source code, but was the effect the same as
that of permissively licensed free software?  Which allows future
versiosn to be proprietary, but the original versions are always free
(such as MIT's X11 or some of the kernels derived from Berkeley UNIX).

[FIXME: the above question is interesting, but it's not clear it
should be left in the paper]

Our next question is about how did early software freedom affect
reproducibility of results in research?

\section{Early scientific software}

Consensus that scientific applications led the computing industry at
this time.  For example, the title of Laning and Zierler's paper was
``A Program for the Translation of Mathematical Equations for
Whirlwind I''.  Nick Trefethen's essay on numerical analysis also
bears that out.

Compilers come in to their own.  After Grace Hopper's early work on
compilers for the UNIVAC, FORTRAN was written for the IBM 704 in 1957
(Ceruzzi p. 90) and COBOL a few years later.

\section{Small computers and the last approaches before the internet}

Usenet and net.sources (later comp.sources and alt.sources).  Before
1985: naive licenses or none at all.  After 1985 a proliferation of
licenses, but mostly well thought out.

BBSes and binary downloads for CP/M and PC-DOS.



Small computers were type-in from magazines.



\section{Nominal fees, zero cost and software freedom}

UNIX was ``nominal fee'' and free to academic institutions, but
licensing had not been thought through until Stallman developed the
GNU General Public License.

The movement of UNIX development from Bell Labs to Berkeley was
important and felt like a great boon, but ...

\section{The arrival of networks}

Arpanet: 11 sites in 1970.

Usenet in 1979.  email and newsgroups.

BBSs in the 1980s.

Ethernet for local-area.

Internet becomes widely available.


\chapter{The stewardship of free and open source software}

Need for large scale coordination of the body of free software.

Compiler toolchain

Linux kernel

Libraries and widgets and other toolkits

Desktops

\section{Organizations with the sole purpose of promoting free and open
  source software}


\section{Organizations which advance free and open source software}

\chapter{Case studies}

\section{Failures and successes in early scientific software}

success: SHARE

failure: clams

success: netlib

success: spice

\section{TeX}

TeX predates the GNU project.

\section{The preprint server}

\section{The GNU Scientific Library}

\section{The python world}

\section{Longevity of data acquisition platforms}

\chapter{Quality and reproducibility}

Leapfrogging of gcc quality vs. the proprietary compilers.

\chapter{Prolegomena for large-scale scientific software projects}

The chapter on case studies could have been written to try to push a
broader message.  We tried to only draw specific conclusions and
illustrate how several broader conclusions could be drawn.

In this chapter we give some ``{\emph do}s and {\emph don't}s'' from
authors and maintainers of significant bodies of scientific software.
They are selected not to be comprehensive, but rather to present
angles on issues that many scientists do not encounter in their
education, and often have to learn through difficult experiences.

Most scientific publications do not dwell on what went wrong and on
the methodological ``lessons learned'', so these lessons are often
part of an oral history.  A younger generation of scientists often
blogs about their methods, and sometimes a journal will feature a
``best practices'' article.

The prolegomena we give here come from scientist from various
generations.  Some are oral reports of talks they have given, others
are references to blog posts and articles.

Titus Brown et. al. paper on ``best practices''.

Kevin McCarty's rant.

Cheng Ho and XMM/Newton.

Tom Tromey and Mark Galassi on quality vs. freedom

\chapter{Importance of FOSS in reproducible research}




\section{Softare: tool or a key part of research?}


Two competing tugs at the researcher: on-the-go workflows, mobile
devices, ... versus longevity, robustness and reproducibility.  How
this affects the training of young researchers.



GUIs versus Makefile/scripting/command-line

Nature ``comment'' on ``Check for publication integrity before
misconduct'': the article is interesting, but it does not address
software issues at all.  This is clearly a problem.

Laura's comment on this ussue: ``I suspect it's partly a consequence
that in many fields people do not receive relevant training s/w is
viewed as a tool, vs part of the process of doing science you know,
like a wordprocessor is a tool you use to write the paper vs. an
integral part of the process''

Laura then points out: http://plain-text.co/

This might also releate to Kevin McCarty's rant.  It might also relate
to Fred Brooks's comments on how engineering ``enters'' the process of
software to make it more ... (reliable? ... FIXME: look up that
chapter in The Mythical Man Month)

\section{Cultural vs. technical aspects}

Difference between cultural and technical connection between source
code availability (and permanent availability/redistributability).

Disctinction between code quality and scientific reproducibility.

Capability maturity models (CMMs) are preached but not practiced by
working research.  Typical science approach to reproducing via
software is the one to be investigated.

\section{Formal verification and validation}

Verification and validation is carried out in the world of serious
large-scale scientific projects: examples from DOE and DOD.  acronyms:
VV\&A VVT\&E.  Best example of the importance of V\&V might be the RAND
corporation's report on the readiness of the US Department of Defense
in AI research \autocite[p.6]{tarraf2019department}.  Enthusiasm for
APIs and FOSS is mentioned at page 6.  This is a cultural rather than
technical level of FOSS requirement.

https://www.rand.org/pubs/research\_reports/RR4229.html

https://www.meritalk.com/articles/pentagon-not-yet-ready-for-prime-time-ai-rand-report-says/

At the technical level look for citations of Livermore's V\&V of Los
Alamos's code.


\printbibliography

\end{document}
