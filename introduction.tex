\section{Motivation and plan}
\label{sec:plan}

Sharing software is to computing what sharing recipes is to
cooking \parencite[53]{Stallman_1999}. Just as individual cooks may be
more or less willing to reveal the secret ingredient to a favourite
dish, individual coders may be more or less inclined to make their
programs available to others. Personal dispositions aside, programmers
also have to grapple with what has been dubbed ``[t]he fundamental
tension---how to publish software, comment on it, encourage learning
from it, yet still retain commercial and technical
control'' \parencite{Ritchie_1996}.

This tension arises from the opposition of two camps, which can be
caricatured as follows. One maintains that code ought to be accessible
and shared with no restrictions; the other insists that code be
closely guarded for the sake of intellectual property and, more
prosaically, in the prospect of turning that property into profit
\parencite{Ritchie_1996}. What we now term ``free and open source
software'' (FOSS) emerged piecemeal, starting in the 1970s, along a
path unfolding between these two camps.

Nowadays, many researchers are no more than a few clicks or taps away
from a large body of literature, data, and software. It can therefore
be hard for them to imagine just how different the process of
accessing those resources was even 10 years ago --- let alone in the
early 1970s, when the tension between the two camps began to manifest.
Possibly as a result of this discrepancy in day-to-day experience,
only a minority across the research community today seem to appreciate
the close ties between FOSS and academia. Exceptions include those who
work at the interface with FOSS, and a handful of others with a
specific interest in the topic. For example, few in the community seem
aware that key elements of FOSS emerged in a culture of openness and
collective effort, in keeping with long-established academic
principles of knowledge-sharing. Fewer still seem to realize that FOSS
provides the technical backbone of the infrastructure that enables
their effortless access to literature, data, and software (\eg
high-speed networks, responsive search engines).

In fact, today there is often even confusion among researchers as to
what FOSS actually is. A case in point is the common interpretation of
``open source'' as ``open code'' --- that is, software for which the
human-readable form of the code, or source code, is openly available,
with unrestricted access by all. Misled perhaps by similarity in the
name (think, for example, of open access in academic publishing),
advocates of open science have turned their attention to the software
that generates findings reported in scientific papers. In this
context, availability of code is widely regarded as essential to
reproducibility of the results \parencite[\eg][]{Stodden-et-al_2013,
  Stodden-et-al_2018, Culina-et-al_2020}. Therefore, a common
recommendation is that researchers make the software ``open source'',
by hosting it in an online repository that is publicly
accessible \parencite[\eg][]{Jimenez-et-al_2017}. And yet public
access to the source code is neither necessary nor sufficient for a
computer program to qualify as FOSS.

This paper is aimed at researchers with an interest in FOSS, and
specifically in the intersection between FOSS and reproducibility. To
the extent that reproducibility is linked to research quality and
sustainability, the discussion is relevant to several others in the
research community --- including support staff, administrators,
publishers, funders, and any other group with a stake in the
improvement of research
culture \parencite{Munafo-et-al_2020}. Accordingly, we begin by
providing definitions of reproducibility and related notions, followed
by the argument that the discourse surrounding FOSS needs extending to
encompass research and scholarship in the broadest sense
(Section~\ref{sec:scene}).

The rest of the paper comprises two substantive parts. The first is a
primer to FOSS (Section~\ref{sec:primer}). The material is pitched at
novices, but it is also suitable for readers beyond this group --- for
example, researchers who have some familiarity with relevant terms and
concepts, but who are now wondering whether the code they shared in a
public repository hosted on, say, \url{https://github.com/} is indeed
FOSS. We encourage anyone puzzled by this example to engage with
Sections~\ref{sec:name} and \ref{sec:legal} in particular.

The second part illustrates the relevance of FOSS to the research
community, with reference to the GNU Scientific
Library \parencite{Galassi-et-al_2009, Galassi-et-al_2019, GSL-web} as
a case study (Section~\ref{sec:gsl}). More advanced readers, and
anyone with a specific interest in numerical software for scientific
computing, can skip to this part.

We conclude with brief remarks about the role of FOSS in research and
scholarship (Section~\ref{sec:closing}), including the ``solution'' to
the puzzle above about software hosted publicly online.

Throughout, we draw on our experience advocating for FOSS, which
collectively spans several decades across multiple professional
contexts, different countries, and disparate fields beyond our own.

\section{Setting the scene}
\label{sec:scene}

There is ambiguity, and some confusion, in terminology linked to
reproducibility and related notions. We rely on the definitions
provided in the recent report on \citetitle{NASEM_2019} by the US
\citeauthor{NASEM_2019} \parencite{NASEM_2019}. Here,
\emph{reproducibility} is defined as ``obtaining consistent
computational results using the same input data, computational steps,
methods, code, and conditions of
analysis'' \parencite[1]{NASEM_2019}. Reproducibility is distinct from
\emph{replicability}, defined as ``obtaining consistent results across
studies aimed at answering the same scientific question, each of which
has obtained its own data'' \parencite[1]{NASEM_2019}. In turn, both
are distinct from \emph{generalizability}, namely ``the extent that
results of a study apply in other contexts or populations that differ
from the original one'' \parencite[1--2]{NASEM_2019}.

In line with the remit of the issuing organization, the report relates
to computational reproducibility specifically with respect to
scientific research \parencite{NASEM_2019}. By contrast, our interest
is in research generally, which we define loosely as the process of
knowledge production and dissemination. Anyone who actively
contributes to this process today is effectively a \emph{computational
  researcher}, to the extent that they use computers to manage and
store information. This designation shifts the emphasis from viewing
software as a tool, to viewing it as an integral part of the
day-to-day workflow for any researcher in any field. By broadening the
scope of the discussion in this way, we aim to highlight the relevance
of FOSS to disciplines in which the software-as-tool perspective has
tended to prevail --- including, for example, many fields in the
humanities and in the social sciences, and some fields in the natural
sciences, in engineering, and in medicine. In other words, we seek to
extend the discourse surrounding FOSS from the usual context of
\emph{open science} to the broader context of \emph{open research}.

The rationale here is that FOSS is relevant to research practice
beyond the domain of scientific programming: any researcher in any
field is a computational researcher, whether or not they engage in
software development. Against this rationale, a further step is to
extend the discourse surrounding FOSS from open research to \emph{open
  scholarship} --- that is, to encompass both the research process
itself and the various resources (\eg intellectual, financial,
administrative) that enable it. To our knowledge, related discussions
to date have focused on the interplay between FOSS and open access in
academic publishing \parencite[\eg][]{Willinsky_2006}. Yet with the
ever-growing reliance on digital tools and services across the full
range of research activity, it seems timely to explore the role of
FOSS in other domains --- for example, as applied to the systems used
to publish scholarly content and teaching materials, or to those used
for the day-to-day management of research funding. In our experience,
the conversation is often framed in terms of trade-offs between the
cost-effectiveness of FOSS solutions and the convenience of non-FOSS
products. Such narrow framing obscures the many parallels between FOSS
and open scholarship, and their convergence of interests in
contributing to the public good \parencite{Willinsky_2005}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
