%%% Local Variables:
%%% TeX-master: "foss-in-scientific-research"
%%% End:

\section{Importance of FOSS in reproducible and long-lasting research}
\label{sec:importance-of-foss-in-reproducible}

We believe that the the software used in reproducible research must be
free, and this point is often argued elsewhere as well.  But we do not
believe that the argument has been nuanced enough to be fully
convincing.

In this section we look at our case study from
Section~\ref{sec:case-study-the-gnu-scientific-library} and the
details of the link between software licensing and the desired outcome
of high quality reproducible research.  Our goal is to answer the
question ``did it have to be FOSS?''

We also discuss the importance of source code licensing for scientific
projects that have a long life cycle.

\subsection{Opposition to free software licensing in academic circles}

Although academicians are typically inclined toward sharing research
software, there are two pressures that tug in the other direction.

One is a certain embarrassment about the underlying code: the thought
that it was written to meet a deadline, and that it could be made so
much better.  A scientist might wait for years to prioritize the
cleanup of code.  At this point the code might be irrelevant, and the
window for peer review of the scientific output will be long gone.

Another influential factor is the US government's prescription on how
software developed at national laboratories should be distributed.  In
principle all software developed by the US government should be
released into the public domain, and hence be free software.  But in
managing the scientific research labs the government uses a
``government owned, contractor operated'' approach, where a separate
company manages the laboratory.  The ``invention rights'' are granted
to the company that operates the laboratory, and these companies often
see this as an opportunity to make significant additional revenue from
their management contracts, with entire divisions of laboratories
dedicated to attempts at commercializing publicly funded softare.

For a few decades there was an unfortunate decrease in the amount of
free software from the government, although in recent years the
widespread recognition of the importance of FOSS has led to
streamlined procedures for releasing much of a government scientist's
source code under a free license.

\subsection{Necessity of source code}

Without source code nobody can verify that the algorithm that leads
from data (or simulation parameters) to a scientific conclusion is
valid.  This means that \emph{reproducible research} (or we would say
simply \emph{research}) requires availability of source code.

Long-running projects have another path to requiring source code.  A
space observatory, for example, or a particle physics accelerator, or
a long-running survey of atomspheric weather or space weather: all of
these projects will use hardware for years after it was first
procured.  In many cases the companies that built the hardware will
have gone out of business, or changed marketing focus, while there is
still a need to change and adapt the operating software.  The orphaned
software ends up being a burden which can cripple experimental
missions.

\subsection{Are all the freedoms necessary?}
\label{subsec:are-all-the-freedoms-necessary}

There are various ways of making source code available.  Two
approaches that do not involve a FOSS license are:

\begin{description}
\item[Source code escrow] A copy of the source code is given to a
  third party company which will have permission to maintain the
  software if the original developer fails to maintain and update the
  software.
\item[Proprietary source release] An author can release software with
  source code, but also write the license to restrict the
  redistribution of the source.  Examples of restrictions are \emph{no
    modification}, \emph{no redistributing of modified copies}, and
  \emph{no commercial use}.
\end{description}

Source code escrow is not a solution for scientific missions or data
analysis software: the need for immediate and continuous peer scrutiny
requires source code for the entire process (not just after the
company fails).  In addition there are pitfalls in the escrow contract
details, with the real possibility of the software not being usable to
reproduce an experimental pipeline.

Proprietary source releases allow full inspection of the software, but
some problems come up.  The first is that there is no guarantee that
the source code is the same that was used to build the specific
binaries.  In addition, all of the stated restrictions placed on the
use of the source inhibit programmers or companies that might arise to
pick up maintainance of orphaned software: their motivation would be
further commercialization of the software, which is not possible under
a proprietary license.

Thus all of the four freedoms specified by the Free Software
Foundation's definition of free software are needed to guarantee
scientific reproducibility and longevity.

It is worth noting another early insight by Richard Stallman: the
General Public License requires that the source code released to users
also include the tools required to build the shipped binaries:
\begin{quote}
\emph{The ``Corresponding Source'' for a work in object code form
  means all the source code needed to generate, install, and (for an
  executable work) run the object code and to modify the work,
  including scripts to control those activities.} \autocite{gplv3}
\end{quote}
this is a key prerequisite to scientific verification.


\subsection{Cultural factors and traditions}

We have mentioned the ``open development model'', while at the same
time pointing out that it is important to rely on precisely defined
terms like ``free software'' and ``open source software'', and to
treat terminology such as ``open'' as adding colour, but not being a
key part of the process of doing research.

% But here we do return to discussing some cultural aspects of the free
% software tradition, mostly to discuss the lineage of {\it de facto}
% practices in well-run scientific software projects.  In
% Section~\nestedreftwo{sec:importance-of-foss-in-reproducible}{subsec:are-all-the-freedoms-necessary}
% we point out that software freedom is a necessary component of
% reproducible science \emph{based on the technicalities of the four
%   freedoms}.  But another argument for this is indeed that of a
% tradition of rigorous software practices.

% % FIXME possibly move as a separate subsection to GSL section,
% % also incoporating discussion of GNU autotools currently below?
% % 
% Toward the end of
% Section~\nestedreftwo{sec:case-study-the-gnu-scientific-library}{subsec:the-design}
% we pointed out that the GNU Scientific Library (GSL) design and
% practice of implementation were robust and have contributed to GSL's
% longevity.  But the GSL team did not invent those practices: the
% authors were simply inspired by the benefits of what they saw in the
% classical free software projects -- the emacs editor, the GNU
% compilers, the Linux kernel, \dots

% The GNU coding standards date back to at least 1992 and they highlight
% the difference between amateurish and well-engineered software:
% software developed by programmers steeped in the GNU project tends to
% automatically ``get right'' many important aspects of the process.

Other traditions of the free software movement are now baked in to
modern software engineering workflows.  As an example: all the
significant version control systems in use today came out of the free
software movement.  Since free software, by definition, exposes its
source code, doing so via version control was a short step for most
projects.

Donald Knuth's ``trip and trap'' tests for the \TeX\ typesetting
system \autocite{knuth1984torture} give us another example of good
software development methodology which took root early in the free
software tradition.
  
% FIXME would discussion of cathedral and bazaar be relevant here?
% e.g. according to Raymond, bazaar approach was instrumental to
% successful development of Linux kernel

\subsection{Just another tool, or a key part of research?}

Today nobody disputes that custom software is a key part of any
computational research project.  But there is a spectrum of rigour
with which researchers write custom software.  Some projects have a
team member with software engineering experience, while others might
be adapting scripts they found somewhere.  And although there is a
perception of hierarchy (physics to chemistry to biology to social
sciences), the truth is that in all sciences you find practitioners in
all areas of that spectrum of rigour.

One factor that positions a scientist on this spectrum is training in
advanced computing techniques, but another one is just as important:
the \emph{competing lures:}

A researcher would like her code to have longevity, robustness,
reproducibility, but she is also tempted by on-the-go workflows, use
of mobile devices, \dots all of which are incompatible with careful
reproducible research.  Many scientists choose the latter, and under
time pressure even more will do so.

The approach of advanced training has seen progress thanks to the
publication of excellent books and tutorials
\autocite{haddock2011practical,wilson2014software,healy2018plain}.
The results have been excellent, with many young researchers in social
sciences developing intimacy and comfort with their software tools.

The temptation of on-the-go workflows and mobile devices can be
addressed in part with \emph{exhortation} articles such as this one
and those cited in
Section~\ref{sec:prolegomena-for-future-research-software}.  These
articles give a (usually long) list of prescriptions for software and
data management practices.  They form the ``low-hanging fruit'' of
software quality: you achieve a good fraction of your goals, while
still being able to compute with a comfortable paradigm.

For completeness we mention a different \emph{checklist-based}
approach: Grey and others examine the issue of publication integrity,
but they focus on human factors rather than computational pipelines
\autocite{grey2020check}.  They draw up a (human) checklist designed
to find a variety of incorrect or even fraudolent practices.
